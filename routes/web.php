<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'web'], function() {
    Route::get('/login', ['as' => 'login', 'uses' => 'AuthController@login']);
    Route::post('italent/', ['as' => 'authenticate', 'uses' => 'AuthController@authenticate']);
    Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout', 'middleware' => ['auth']]);

//    Route::get('/', function () {
//        return view('home/index');
//    });

    Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index', 'middleware' => ['auth']]);

    Route::get('/competencia', ['as' => 'index', 'uses' => 'CompetenciaController@index', 'middleware' => ['auth']]);
    Route::get('competencia/cadastro', ['as' => 'cadastro', 'uses' => 'CompetenciaController@cadastro', 'middleware' => ['auth']]);
    Route::post('competencia/cadastro', ['as' => 'competencia.create', 'uses' => 'CompetenciaController@create', 'middleware' => ['auth']]);
    Route::post('competencia/cadastro', ['as' => 'competencia.create', 'uses' => 'CompetenciaController@create', 'middleware' => ['auth']]);
    Route::get('competencia/edita/{id}', ['as' => 'competencia.edita', 'uses' => 'CompetenciaController@edita', 'middleware' => ['auth']]);
    Route::post('competencia/update', ['as' => 'competencia.update', 'uses' => 'CompetenciaController@update', 'middleware' => ['auth']]);
    Route::delete('competencia/destroy', ['as' => 'competencia.destroy', 'uses' => 'CompetenciaController@destroy', 'middleware' => ['auth']]);
//Route::get('pessoa/cadastro', ['as' => 'cadastro', 'uses' => 'CompetenciaController@cadastro', 'middleware' => ['auth']]);

    Route::get('competencia/itens/{id}', ['as' => 'competencia.itens', 'uses' => 'CompetenciaController@itens', 'middleware' => ['auth']]);
    Route::get('competencia_item/cadastro_item/{id}', ['as' => 'competencia_item.cadastro_item', 'uses' => 'CompetenciaController@cadastro_item', 'middleware' => ['auth']]);
    Route::post('competencia_item/cadastro_item', ['as' => 'competencia_item.create_item', 'uses' => 'CompetenciaController@create_item', 'middleware' => ['auth']]);
    Route::get('competencia_item/edita_item/{id}', ['as' => 'competencia_item.edita_item', 'uses' => 'CompetenciaController@edita_item', 'middleware' => ['auth']]);
    Route::post('competencia_item/update_item', ['as' => 'competencia_item.update_item', 'uses' => 'CompetenciaController@update_item', 'middleware' => ['auth']]);
    Route::delete('competencia_item/destroy_item', ['as' => 'competencia_item.destroy_item', 'uses' => 'CompetenciaController@destroy_item', 'middleware' => ['auth']]);


    Route::get('/especializacao', ['as' => 'index', 'uses' => 'EspecializacaoController@index', 'middleware' => ['auth']]);
    Route::get('especializacao/cadastro', ['as' => 'cadastro', 'uses' => 'EspecializacaoController@cadastro', 'middleware' => ['auth']]);
    Route::post('especializacao/cadastro', ['as' => 'especializacao.create', 'uses' => 'EspecializacaoController@create', 'middleware' => ['auth']]);
    Route::post('especializacao/cadastro', ['as' => 'especializacao.create', 'uses' => 'EspecializacaoController@create', 'middleware' => ['auth']]);
    Route::get('especializacao/edita/{id}', ['as' => 'especializacao.edita', 'uses' => 'EspecializacaoController@edita', 'middleware' => ['auth']]);
    Route::post('especializacao/update', ['as' => 'especializacao.update', 'uses' => 'EspecializacaoController@update', 'middleware' => ['auth']]);
    Route::delete('especializacao/destroy', ['as' => 'especializacao.destroy', 'uses' => 'EspecializacaoController@destroy', 'middleware' => ['auth']]);

    Route::get('especializacao/assunto/{id}', ['as' => 'especializacao.assuntos', 'uses' => 'EspecializacaoController@assuntos', 'middleware' => ['auth']]);
    Route::get('especializacao_assunto/cadastro_assunto/{id}', ['as' => 'especializacao_assunto.cadastro_assunto', 'uses' => 'EspecializacaoController@cadastro_assunto', 'middleware' => ['auth']]);
    Route::post('especializacao_assunto/cadastro_assunto', ['as' => 'especializacao_assunto.create_assunto', 'uses' => 'EspecializacaoController@create_assunto', 'middleware' => ['auth']]);
    Route::get('especializacao_assunto/edita_assunto/{id}', ['as' => 'especializacao_assunto.edita_assunto', 'uses' => 'EspecializacaoController@edita_assunto', 'middleware' => ['auth']]);
    Route::post('especializacao_assunto/update_assunto', ['as' => 'especializacao_assunto.update_assunto', 'uses' => 'EspecializacaoController@update_assunto', 'middleware' => ['auth']]);
    Route::delete('especializacao_assunto/destroy_assunto', ['as' => 'especializacao_assunto.destroy_assunto', 'uses' => 'EspecializacaoController@destroy_assunto', 'middleware' => ['auth']]);


    Route::get('/pessoa', ['as' => 'index', 'uses' => 'PessoaController@index', 'middleware' => ['auth']]);
    Route::get('pessoa/cadastro', ['as' => 'cadastro', 'uses' => 'PessoaController@cadastro', 'middleware' => ['auth']]);
    Route::post('pessoa/cadastro', ['as' => 'pessoa.create', 'uses' => 'PessoaController@create', 'middleware' => ['auth']]);
    Route::post('pessoa/cadastro', ['as' => 'pessoa.create', 'uses' => 'PessoaController@create', 'middleware' => ['auth']]);
    Route::get('pessoa/edita/{id}', ['as' => 'pessoa.edita', 'uses' => 'PessoaController@edita', 'middleware' => ['auth']]);
    Route::post('pessoa/update', ['as' => 'pessoa.update', 'uses' => 'PessoaController@update', 'middleware' => ['auth']]);
    Route::delete('pessoa/destroy', ['as' => 'pessoa.destroy', 'uses' => 'PessoaController@destroy', 'middleware' => ['auth']]);

    Route::get('/qualificacao', ['as' => 'index', 'uses' => 'QualificacaoController@index', 'middleware' => ['auth']]);
    Route::get('qualificacao/cadastro', ['as' => 'cadastro', 'uses' => 'QualificacaoController@cadastro', 'middleware' => ['auth']]);
    Route::post('qualificacao/cadastro', ['as' => 'qualificacao.create', 'uses' => 'QualificacaoController@create', 'middleware' => ['auth']]);
    Route::post('qualificacao/cadastro', ['as' => 'qualificacao.create', 'uses' => 'QualificacaoController@create', 'middleware' => ['auth']]);
    Route::get('qualificacao/edita/{id}', ['as' => 'qualificacao.edita', 'uses' => 'QualificacaoController@edita', 'middleware' => ['auth']]);
    Route::post('qualificacao/update', ['as' => 'qualificacao.update', 'uses' => 'QualificacaoController@update', 'middleware' => ['auth']]);
    Route::delete('qualificacao/destroy', ['as' => 'qualificacao.destroy', 'uses' => 'QualificacaoController@destroy', 'middleware' => ['auth']]);

    Route::get('/tipo_lideranca', ['as' => 'index', 'uses' => 'TipoLiderancaController@index', 'middleware' => ['auth']]);
    Route::get('tipo_lideranca/cadastro', ['as' => 'cadastro', 'uses' => 'TipoLiderancaController@cadastro', 'middleware' => ['auth']]);
    Route::post('tipo_lideranca/cadastro', ['as' => 'tipo_lideranca.create', 'uses' => 'TipoLiderancaController@create', 'middleware' => ['auth']]);
    Route::post('tipo_lideranca/cadastro', ['as' => 'tipo_lideranca.create', 'uses' => 'TipoLiderancaController@create', 'middleware' => ['auth']]);
    Route::get('tipo_lideranca/edita/{id}', ['as' => 'tipo_lideranca.edita', 'uses' => 'TipoLiderancaController@edita', 'middleware' => ['auth']]);
    Route::post('tipo_lideranca/update', ['as' => 'tipo_lideranca.update', 'uses' => 'TipoLiderancaController@update', 'middleware' => ['auth']]);
    Route::delete('tipo_lideranca/destroy', ['as' => 'tipo_lideranca.destroy', 'uses' => 'TipoLiderancaController@destroy', 'middleware' => ['auth']]);

    Route::get('tipo_lideranca/itens/{id}', ['as' => 'tipo_lideranca.itens', 'uses' => 'TipoLiderancaController@itens', 'middleware' => ['auth']]);
    Route::get('item_tipo_lideranca/cadastro_item/{id}', ['as' => 'item_tipo_lideranca.cadastro_item', 'uses' => 'TipoLiderancaController@cadastro_item', 'middleware' => ['auth']]);
    Route::post('item_tipo_lideranca/cadastro_item', ['as' => 'item_tipo_lideranca.create_item', 'uses' => 'TipoLiderancaController@create_item', 'middleware' => ['auth']]);
    Route::get('item_tipo_lideranca/edita_item/{id}', ['as' => 'item_tipo_lideranca.edita_item', 'uses' => 'TipoLiderancaController@edita_item', 'middleware' => ['auth']]);
    Route::post('item_tipo_lideranca/update_item', ['as' => 'item_tipo_lideranca.update_item', 'uses' => 'TipoLiderancaController@update_item', 'middleware' => ['auth']]);
    Route::delete('item_tipo_lideranca/destroy_item', ['as' => 'item_tipo_lideranca.destroy_item', 'uses' => 'TipoLiderancaController@destroy_item', 'middleware' => ['auth']]);

    Route::get('/ciclo', ['as' => 'index', 'uses' => 'CicloController@index', 'middleware' => ['auth']]);
    Route::get('ciclo/cadastro', ['as' => 'cadastro', 'uses' => 'CicloController@cadastro', 'middleware' => ['auth']]);
    Route::post('ciclo/cadastro', ['as' => 'ciclo.create', 'uses' => 'CicloController@create', 'middleware' => ['auth']]);
    Route::post('ciclo/cadastro', ['as' => 'ciclo.create', 'uses' => 'CicloController@create', 'middleware' => ['auth']]);
    Route::get('ciclo/edita/{id}', ['as' => 'ciclo.edita', 'uses' => 'CicloController@edita', 'middleware' => ['auth']]);
    Route::post('ciclo/update', ['as' => 'ciclo.update', 'uses' => 'CicloController@update', 'middleware' => ['auth']]);
    Route::delete('ciclo/destroy', ['as' => 'ciclo.destroy', 'uses' => 'CicloController@destroy', 'middleware' => ['auth']]);
//Route::get('pessoa/cadastro', ['as' => 'cadastro', 'uses' => 'CompetenciaController@cadastro', 'middleware' => ['auth']]);

    Route::get('ciclo/itens/{id}', ['as' => 'ciclo.itens', 'uses' => 'CicloController@itens', 'middleware' => ['auth']]);
    Route::get('ciclo_item/cadastro_item/{id}', ['as' => 'ciclo_item.cadastro_item', 'uses' => 'CicloController@cadastro_item', 'middleware' => ['auth']]);
    Route::post('ciclo_item/cadastro_item', ['as' => 'ciclo_item.create_item', 'uses' => 'CicloController@create_item', 'middleware' => ['auth']]);
    Route::get('ciclo_item/edita_item/{id}', ['as' => 'ciclo_item.edita_item', 'uses' => 'CicloController@edita_item', 'middleware' => ['auth']]);
    Route::post('ciclo_item/update_item', ['as' => 'ciclo_item.update_item', 'uses' => 'CicloController@update_item', 'middleware' => ['auth']]);
    Route::delete('ciclo_item/destroy_item', ['as' => 'ciclo_item.destroy_item', 'uses' => 'CicloController@destroy_item', 'middleware' => ['auth']]);

    Route::get('ciclo/talentos', ['as' => 'ciclo.talentos', 'uses' => 'CicloController@talentos', 'middleware' => ['auth']]);
    Route::post('ciclo/talentos', ['as' => 'ciclo.postTalentos', 'uses' => 'CicloController@postTalentos', 'middleware' => ['auth']]);


    Route::get('ciclo/export/{id}', ['as' => 'ciclo.export', 'uses' => 'CicloController@export', 'middleware' => ['auth']]);
    Route::get('ciclo/export/talento/{id}', ['as' => 'ciclo.exportTalento', 'uses' => 'CicloController@exportTalento', 'middleware' => ['auth']]);
    Route::get('ciclo/export/talento/pdf/{id}', ['as' => 'ciclo.pdfTalento', 'uses' => 'CicloController@pdfTalento', 'middleware' => ['auth']]);

    Route::get('ciclo/itens/import/{id}', ['as' => 'ciclo_item.import', 'uses' => 'CicloController@import', 'middleware' => ['auth']]);
    Route::post('ciclo/itens/import', ['as' => 'ciclo_item.importRegistro', 'uses' => 'CicloController@importRegistro', 'middleware' => ['auth']]);


    Route::get('/usuarios', ['as' => 'index', 'uses' => 'UsuarioController@index', 'middleware' => ['auth']]);
    Route::get('usuarios/cadastro', ['as' => 'cadastro', 'uses' => 'UsuarioController@cadastro', 'middleware' => ['auth']]);
    Route::post('usuarios/cadastro', ['as' => 'usuarios.create', 'uses' => 'UsuarioController@create', 'middleware' => ['auth']]);
    Route::get('usuarios/edita/{id}', ['as' => 'usuarios.edita', 'uses' => 'UsuarioController@edita', 'middleware' => ['auth']]);
    Route::post('usuarios/update', ['as' => 'usuarios.update', 'uses' => 'UsuarioController@update', 'middleware' => ['auth']]);
    Route::delete('usuarios/destroy', ['as' => 'usuarios.destroy', 'uses' => 'UsuarioController@destroy', 'middleware' => ['auth']]);
});
