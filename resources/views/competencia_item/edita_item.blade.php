@extends('layout')
@section('title')
Edição do item da competência
@endsection
@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="portlet light ">

    <div class="portlet-body form">
        {{ Form::open(array('route' => 'competencia_item.update_item', 'class' => 'form-horizontal', 'role' => 'form', 'data-mark-field' => 'true', 'data-error-display-class'=>'.alert-danger')) }}   
        <div class="form-body">
            <div class="form-group">
                {{ Form::label('descricao', 'Descrição', array('class' => 'col-md-1 control-label')) }}                
                <div class="col-md-9">
                    {{ Form::text('descricao', $objeto->descricao, array('class' => 'form-control uppercase', 'placeholder' => '')) }}
                </div>
            </div>                
        </div>
        {{ Form::hidden('codigo_iten_competencia', $objeto->codigo_iten_competencia) }}
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-1 col-md-9">
                    <button type="submit" id="showtoast" class="btn blue btn-primary">Salvar</button>
                    <a href="{{url('competencia/itens',$objeto->competencia_codigo_competencia)}}" class="btn red btn-outline ">
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

</div>

@endsection






