@extends('layout')
@section('title')
Cadastro da Especialização
@endsection
@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="portlet light ">

    <div class="portlet-body form">
        {{ Form::open(array('route' => 'ciclo_item.importRegistro', 'class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data', 'data-mark-field' => 'true', 'data-error-display-class'=>'.alert-danger')) }}   
        <div class="form-body">
            <div class="form-group">
                {{ Form::label('file', 'Arquivo', array('class' => 'col-md-1 control-label')) }}                
                <div class="col-md-9">
                    {{ Form::file('file', ['class' => 'form-control uppercase']) }}
                </div>
            </div>                
        </div>
        <div class="form-actions">
            <div class="row">
                {{ Form::hidden('ciclo_codigo', $codigo_ciclo) }}
                <div class="col-md-offset-1 col-md-9">
                    <button type="submit" id="showtoast" class="btn blue btn-primary">Importar</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

</div>

@endsection






