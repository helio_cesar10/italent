@extends('layout')
@section('css')

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/jquery-multi-select/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('js')
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-toastr.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-multi-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/ciclo/cadEdi.js') }}"></script>

@endsection
@section('title')
Cadastro do Talento
@endsection
@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="portlet light ">

    <div class="portlet-body form">
        {{ Form::open(array('route' => 'ciclo_item.create_item', 'class' => 'form-horizontal', 'role' => 'form', 'data-mark-field' => 'true', 'data-error-display-class'=>'.alert-danger')) }}   
        <div class="form-body">
            <div class="form-group">
                {{ Form::label('colaborador', 'Ciclo', array('class' => 'col-md-1 control-label')) }}                
                <div class="col-md-5">
                    {{ Form::text('ciclo', $objeto->descricao, array('class' => 'form-control uppercase', 'readonly' => 'readonly')) }}
                </div>
            </div>                
            <div class="form-group">
                {{ Form::label('colaborador', 'Colaborador', array('class' => 'col-md-1 control-label')) }}                
                <div class="col-md-7">
                    {{ Form::select('colaborador', $colaboradores, null,['class' => 'form-control uppercase', 'id'=> 'colaborador', 'required'=>true]) }}
                </div>
            </div> 
            <div class="form-group">
                {{ Form::label('competencias', 'Competência', array('class' => 'col-md-1 control-label')) }}
                <div class="col-md-7 uppercase">
                    {{ Form::select('competencias', $competencias, null, [ 'class' => 'form-control multi-select uppercase', 'multiple' =>"multiple",'id'=>"my_multi_select1", 'name'=>"competencias[]", 'required'=>true]) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('especializacao', 'Especialização', array('class' => 'col-md-1 control-label')) }}
                <div class="col-md-7 uppercase">
                    {{ Form::select('especializacao', $especializacao, null, [ 'class' => 'form-control multi-select uppercase', 'multiple' =>"multiple",'id'=>"my_multi_select2", 'name'=>"especializacao[]", 'required'=>true]) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('qualificacao', 'Qualificação', array('class' => 'col-md-1 control-label')) }}
                <div class="col-md-7 uppercase">
                    {{ Form::select('qualificacao', $qualificacao, null, [ 'class' => 'form-control multi-select uppercase', 'multiple' =>"multiple",'id'=>"my_multi_select3", 'name'=>"qualificacao[]", 'required'=>true]) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('tipo_lideranca', 'Tipo Liderança', array('class' => 'col-md-1 control-label')) }}
                <div class="col-md-7 uppercase">
                    {{ Form::select('tipo_lideranca', $tipo_lideranca, null, [ 'class' => 'form-control multi-select uppercase', 'multiple' =>"multiple",'id'=>"my_multi_select4", 'name'=>"tipo_lideranca[]", 'required'=>true]) }}
                </div>
            </div>
        </div>                          
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-1 col-md-9">
                {{ Form::hidden('ciclo_codigo', $objeto->codigo_ciclo) }}
                <button type="submit" id="showtoast" class="btn blue btn-primary">Salvar</button>
                <a href="{{url('ciclo/itens',$id)}}" class="btn red btn-outline ">
                    Cancelar
                </a>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}

</div>

@endsection






