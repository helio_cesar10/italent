@extends('layout')
@section('css')

@endsection
@section('js')
<script src="{{ asset('js/tipo_lideranca/index_item.js')}}"></script>

@endsection
@section('title')
Itens do Tipo de Liderança
@endsection
@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light ">
    <div class="portlet-title">
        <div class="btn-group">
            <a href="{{ url('/tipo_lideranca')}}" class=" btn blue sbold" > Retornar 
                <i class="fa fa-reply"></i>
            </a>
        </div>
        <div class="btn-group">
            <a href="{{url('/item_tipo_lideranca/cadastro_item',$id)}}" class=" btn green sbold" > Novo
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <form id="form" >
            {{ Form::open(array('id' => 'form')) }}   
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                <thead>
                    <tr>
                        <th class="col-lg-1">  Codigo </th>
                        <th> Descrição </th>
                        <th class="col-lg-1"> Ação </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objetos as $objeto)
                    <tr class="odd gradeX">
                        <td class="uppercase"> {{ $objeto->codigo_item_tipo_lideranca }} </td>
                        <td class="uppercase"> {{ $objeto->descricao }} </td>
                        <td >                            
                            <div class="row" >
                                <div class="col-md-5 col-sm-5" >
                                    <a href="{{url('/item_tipo_lideranca/edita_item',$objeto->codigo_item_tipo_lideranca)}}" class="btn btn-icon-only blue tooltips" data-placement="top" data-original-title='Editar'>
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>                               
                                <div class="col-md-5 col-sm-5" >
                                    {{ Form::hidden('codigo_item_tipo_lideranca', $objeto->codigo_item_tipo_lideranca, array('id'=>'codigo_competencia', 'data-id'=>$objeto->codigo_item_tipo_lideranca)) }}
                                    {{ Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'btn btn-icon-only red deleteItem tooltips','id' => 'btnDelete', 'data-id'=>$objeto->codigo_item_tipo_lideranca ,'data-tipo_lideranca'=>$objeto->tipo_lideranca_codigo_tipo_lideranca,'data-placement'=>"top", 'data-original-title'=>'Excluir'] ) }}
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ Form::close() }}

    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection
