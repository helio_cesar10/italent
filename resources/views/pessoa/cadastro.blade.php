@extends('layout')
@section('title')
Cadastro da Pessoa
@endsection
@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="portlet light ">

    <div class="portlet-body form">
        {{ Form::open(array('route' => 'pessoa.create', 'class' => 'form-horizontal', 'role' => 'form', 'data-mark-field' => 'true', 'data-error-display-class'=>'.alert-danger')) }}   
        <div class="form-body">
            <div class="form-group">
                {{ Form::label('nome', 'Nome', array('class' => 'col-md-1 control-label')) }}                
                <div class="col-md-9">
                    {{ Form::text('nome', null, array('class' => 'form-control uppercase', 'placeholder' => '')) }}
                </div>
            </div>                
            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::label('cpf', 'CPF', array('class' => 'col-md-3 control-label')) }}                
                        <div class="col-md-9" style="left: 5px;">
                            {{ Form::text('cpf', null, array('class' => 'form-control uppercase', 'placeholder' => '')) }}
                        </div>
                    </div>
                    <div class="col-md-7">
                        {{ Form::label('matricula', 'Matricula', array('class' => 'col-md-2 control-label')) }}                
                        <div class="col-md-5">
                            {{ Form::text('matricula', null, array('class' => 'form-control uppercase', 'placeholder' => '')) }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group">
                {{ Form::label('setor', 'Setor', array('class' => 'col-md-1 control-label')) }}                
                <div class="col-md-9">
                    {{ Form::text('setor', null, array('class' => 'form-control uppercase', 'placeholder' => '')) }}
                </div>
            </div>   
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-1 col-md-9">
                    <button type="submit" id="showtoast" class="btn blue btn-primary">Salvar</button>
                    <a href="{{url('pessoa')}}" class="btn red btn-outline ">
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

</div>

@endsection






