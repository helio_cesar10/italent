@extends('layout')
@section('css')

@endsection
@section('js')
<script src="{{ asset('js/pessoa/index.js')}}"></script>

@endsection

@section('title')
Pessoas
@endsection
@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light ">
    <div class="portlet-title">
        <div class="btn-group">
            <a href="{{url('pessoa/cadastro')}}" class=" btn green sbold" > Novo
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <form id="form" >
            {{ Form::open(array('id' => 'form')) }}   
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                <thead>
                    <tr>
                        <th class="col-lg-1"> Codigo </th>
                        <th> Nome </th>
                        <th> CPF </th>
                        <th> Matricula </th>
                        <th> Setor </th>
                        <th class="col-lg-1"> Ação </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objetos as $objeto)
                    <tr class="odd gradeX">
                        <td class="uppercase"> {{ $objeto->codigo_pessoas }} </td>
                        <td class="uppercase"> {{ $objeto->nome }} </td>
                        <td class="uppercase"> {{ $objeto->cpf }} </td>
                        <td class="uppercase"> {{ $objeto->matricula }} </td>
                        <td class="uppercase"> {{ $objeto->setor }} </td>
                        <td >                            
                            <div class="row" >
                                <div class="col-md-5 col-sm-5" >
                                    <a href="{{url('pessoa/edita',$objeto->codigo_pessoas)}}" class="btn btn-icon-only blue tooltips" data-placement="top" data-original-title='Editar'>
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>                               
                                <div class="col-md-5 col-sm-5" >
                                    {{ Form::hidden('codigo_pessoas', $objeto->codigo_pessoas, array('id'=>'codigo_pessoas', 'data-id'=>$objeto->codigo_pessoas)) }}
                                    {{ Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'btn btn-icon-only red deleteItem tooltips','id' => 'btnDelete', 'data-id'=>$objeto->codigo_pessoas ,'data-placement'=>"top", 'data-original-title'=>'Excluir'] ) }}
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ Form::close() }}

    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection
