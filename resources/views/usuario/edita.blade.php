@extends('layout')
@section('title')
Edição de Usuário
@endsection
@section('content')


@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="portlet light ">

    <div class="portlet-body form">
        {{ Form::open(array('route' => 'usuarios.update', 'class' => 'form-horizontal', 'role' => 'form', 'data-mark-field' => 'true', 'data-error-display-class'=>'.alert-danger')) }}   
        <div class="form-body">

            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::label('nome', 'Nome', array('class' => 'col-md-3 control-label')) }}                
                        <div class="col-md-9" style="left: 5px;">
                            {{ Form::text('nome', $objeto->name, array('class' => 'form-control uppercase', 'placeholder' => '')) }}
                        </div>
                    </div>
                    <div class="col-md-7">
                        {{ Form::label('email', 'E-mail', array('class' => 'col-md-2 control-label')) }}                
                        <div class="col-md-6">
                            {{ Form::email('email', $objeto->email, array('class' => 'form-control uppercase', 'placeholder' => '')) }}
                        </div>
                    </div>
                </div>
            </div>                
            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::label('password', 'Senha', array('class' => 'col-md-3 control-label')) }}                
                        <div class="col-md-9" style="left: 5px;">
                            <input type="password" name="password" value="{{$objeto->password}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-7">
                        {{ Form::label('tipo_usuario', 'Tipo Usuário', array('class' => 'col-md-3 control-label')) }}                
                        <div class="col-md-5">
                            {{ Form::select('tipo_usuario', ['1'=>'Administrador', '2'=>'Usuário'], $objeto->tipo_user,['class' => 'form-control uppercase', 'id'=> 'colaborador', 'required'=>true]) }}
                        </div>
                    </div>
                </div>
            </div>                
        </div>
        {{ Form::hidden('id', $objeto->id) }}

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-1 col-md-9">
                    <button type="submit" id="showtoast" class="btn blue btn-primary">Salvar</button>
                    <a href="{{url('usuarios')}}" class="btn red btn-outline ">
                        Cancelar
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

</div>

@endsection






