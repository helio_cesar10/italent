@extends('layout')
@section('css')

@endsection
@section('js')

<script src="{{ asset('js/usuario/index.js')}}"></script>

@endsection
@section('title')
Usuários do Sistema
@endsection
@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light ">
    <div class="portlet-title">
        <div class="btn-group">
            <a href="{{url('usuarios/cadastro')}}" class=" btn green sbold" > Novo
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <form id="form" >
            {{ Form::open(array('id' => 'form')) }}   
            <table class="table table-striped table-bordered table-hover dt-responsive" id="sample_1">
                <thead>
                    <tr>
                        <th class="col-lg-1"> Codigo </th>
                        <th> Nome </th>
                        <th> E-mail </th>
                        <th> Tipo de Usuário </th>
                        <th class="col-lg-1"> Ação </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objetos as $objeto)
                    <tr class="odd gradeX">
                        <td class="uppercase"> {{ $objeto->id }} </td>
                        <td class="uppercase"> {{ $objeto->name }} </td>
                        <td class="uppercase"> {{ $objeto->email }} </td>
                        <td class="uppercase"> 
                            @if($objeto->tipo_user ==  1)
                                ADMINISTRADOR
                            @endif
                            @if($objeto->tipo_user ==  2)
                                USUÁRIO
                            @endif
                        </td>
                        <td >                            
                            <div class="row" >
                                <div class="col-md-5 col-sm-5" >
                                    <a href="{{url('usuarios/edita',$objeto->id)}}" class="btn btn-icon-only blue tooltips" data-placement="top" data-original-title='Editar'>
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>                               
                                <div class="col-md-5 col-sm-5">    
                                    {{ Form::hidden('id', $objeto->id, array('id'=>'id', 'data-id'=>$objeto->id)) }}
                                    {{ Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'btn btn-icon-only red deleteItem tooltips','id' => 'btnDelete', 'data-id'=>$objeto->id ,'data-placement'=>"top", 'data-original-title'=>'Excluir'] ) }}
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ Form::close() }}

    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection
