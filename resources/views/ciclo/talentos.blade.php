@extends('layout')
@section('css')
<link href="{{ asset('css/banco/index.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('js')

<script src="{{ asset('js/ciclo/index_item.js')}}"></script>
<script>
// DataTable
var table = $('#talentos').DataTable({
    scrollX: '800px',
    scrollX: false,
    scrollCollapse: true,
    paging: true,
    fixedColumns: true
});
</script>
@endsection
@section('title')
Banco de Talentos
@endsection
@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->

<div class="portlet light " style="padding: 0px; width: 101%; margin-left: -12px">
    <div class="portlet-title">

        <div class="btn-group" >
            <a href="{{ url('/ciclo/export/talento',$objetos)}}" class=" btn blue sbold" > Exportar xls
                <i class="fa fa-file-excel-o"></i>
            </a>
        </div>
        <div class="btn-group" >
            <a href="{{ url('/ciclo/export/talento/pdf',$objetos)}}" class=" btn blue sbold" >  Imprimir PDF
                <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>
    </div>
    <div class="portlet-body">

        {{ Form::open(array('id' => 'form')) }} 
        <div class="row">
            <div class="form-group col-md-6">
                {{ Form::label('qualificacao', 'Qualificação', array('class' => 'control-label')) }}  
                {{ Form::select('qualificacao', $qualificacao, old('qualificacao') ? old('qualificacao') : 0,['class' => 'form-control uppercase', 'id'=> 'colaborador', 'required'=>true]) }}
            </div>

            <div class="form-group col-md-6">
                {{ Form::label('perfil', 'Perfil', array('class' => 'control-label')) }}                
                {{ Form::select('perfil', $especializacao, old('perfil') ? old('perfil') : 0,['class' => 'form-control uppercase', 'id'=> 'colaborador', 'required'=>true]) }}
            </div>
        </div>
        <div class="row">

            <div class="form-group col-md-6">
                {{ Form::label('competencias', 'Competências', array('class' => 'control-label')) }}                
                {{ Form::select('competencias', $competencias, old('competencias') ? old('competencias') : 0,['class' => 'form-control uppercase', 'id'=> 'colaborador', 'required'=>true]) }}
            </div>

            <div class="form-group col-md-6">
                {{ Form::label('tipo_lideranca', 'Tipo Liderança', array('class' => 'control-label')) }}                
                {{ Form::select('tipo_lideranca', $tipo_lideranca, old('tipo_lideranca') ? old('tipo_lideranca') : 0,['class' => 'form-control uppercase', 'id'=> 'colaborador', 'required'=>true]) }}
            </div>

        </div>        
        <div id="actions" class=" form-group row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary col-lg-offset-11">Pesquisar</button>
            </div>
        </div>

        <table class="table table-striped table-bordered table-hover"  >
            <thead>
                <tr >                    
                    <th id="formata" style="width: 15%;"> Qualificação </th>
                    <th id="formata" style="width: 15%;"> Perfil </th>
                    <th id="formata" style="width: 15%;"> Competência </th>
                    <th id="formata" style="width: 15%;"> Tipo Lideranca </th>  
                    <th id="formata" style="width: 15%;"> Colaborador </th>
                    <th id="formata" style="width: 15%;"> CPF/Matricula </th>
                    <th id="formata" style="width: 15%;"> Setor </th>
                </tr>
            </thead>

            <tbody id="odd">
                @if(empty($objetos))
                <tr class="odd gradeX">

                    <td class="uppercase text-center" colspan="7"> 
                        Nenhun Registro....
                    </td>
                </tr>
                @endif
                @foreach($objetos as $objeto)
                <tr class="odd gradeX">

                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoQualificacao as $cicloQualificacao)
                        <p class="formata">
                            {{ $cicloQualificacao->qualificacao->descricao }} |
                        </p>
                        @endforeach
                    </td>
                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoEspecializacao as $cicloEspecializacao)
                        <p class="formata">
                            {{ $cicloEspecializacao->especializacao->descricao }} |
                        </p>
                        @endforeach
                    </td>
                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoCompetencia as $cicloCompetencia)
                        <p class="formata">
                            <a href="#" class="dcontexto">{{ $cicloCompetencia->competencia->descricao }} 
                                <span>
                                    @foreach($cicloCompetencia->competencia->competencias as $competencia)
                                    {{ $competencia->descricao }} ; 
                                    @endforeach
                                </span>
                            </a> |  
                        </p>                        
                        @endforeach
                    </td>
                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoTipoLideranca as $cicloTipoLideranca)
                        <p class="formata">
                            <a href="#" class="dcontexto"> {{ $cicloTipoLideranca->tipo_lideranca->descricao }} 
                                <span>
                                    @foreach($cicloTipoLideranca->tipo_lideranca->tipo_liderancas as $tipolideranca)
                                    {{ $tipolideranca->descricao }} ; 
                                    @endforeach
                                </span>
                            </a> |  
                        </p>
                        @endforeach
                    </td>   
                    <td class="uppercase"> <p class="formata"> {{ $objeto->colaborador->nome}} </p></td>
                    <td class="uppercase"> <p class="formata"> {{ $objeto->colaborador->cpf}} / {{ $objeto->colaborador->matricula}}  </p></td>
                    <td class="uppercase"> <p class="formata"> {{ $objeto->colaborador->setor}} </p></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <input type="hidden" value="{{ $objetos }}" />
        {{ Form::close() }}

    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection
