@extends('layout')
@section('css')

@endsection
@section('js')

<script src="{{ asset('js/ciclo/index.js')}}"></script>

@endsection
@section('title')
Banco de Talentos
@endsection
@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light ">
    <div class="portlet-title">
        @if(Auth::user()->tipo_user == 1)
        <div class="btn-group">
            <a href="{{url('ciclo/cadastro')}}" class=" btn green sbold" > Novo
                <i class="fa fa-plus"></i>
            </a>
        </div>
        @endif
    </div>
    <div class="portlet-body">
        <form id="form" >
            {{ Form::open(array('id' => 'form')) }}   
            <table class="table table-striped table-bordered table-hover "  id="sample_1">
                <thead>
                    <tr>
                        <th class="col-lg-1">  Codigo </th>
                        <th > Descrição </th>
                        <th class="col-lg-2"> Ação </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objetos as $objeto)
                    <tr class="odd gradeX">
                        <td class="uppercase"> {{ $objeto->codigo_ciclo }} </td>
                        <td class="uppercase"> {{ $objeto->descricao }} </td>

                        <td >                            
                            <div class="row" >
                                <div class="col-md-4 col-sm-4" >
                                    <a href="{{url('ciclo/itens',$objeto->codigo_ciclo)}}" class="btn btn-icon-only green tooltips" data-placement="top" data-original-title='Itens'>
                                        <i class="fa fa-folder-open"></i>
                                    </a>
                                </div>
                                @if(Auth::user()->tipo_user == 1)
                                <div class="col-md-4 col-sm-4" >
                                    <a href="{{url('ciclo/edita',$objeto->codigo_ciclo)}}" class="btn btn-icon-only blue tooltips" data-placement="top" data-original-title='Editar'>
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4" >
                                    {{ Form::hidden('codigo_ciclo', $objeto->codigo_ciclo, array('id'=>'codigo_ciclo', 'data-id'=>$objeto->codigo_ciclo)) }}
                                    {{ Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'btn btn-icon-only red deleteItem tooltips','id' => 'btnDelete', 'data-id'=>$objeto->codigo_ciclo ,'data-placement'=>"top", 'data-original-title'=>'Excluir'] ) }}
                                </div>   
                                @endif
                            </div>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ Form::close() }}

    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection
