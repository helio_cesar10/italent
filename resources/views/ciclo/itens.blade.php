@extends('layout')
@section('css')
<link href="{{ asset('css/banco/index.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('js')

<script src="{{ asset('js/ciclo/index_item.js')}}"></script>
@endsection
@section('title')
Itens do Banco de Talento -- {{$objeto->descricao}}
@endsection
@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->

<div class="portlet light " style="padding: 0px; width: 101%; margin-left: -12px">
    <div class="portlet-title">
        <div class="btn-group">
            <a href="{{ url('/ciclo')}}" class=" btn blue sbold" > Retornar 
                <i class="fa fa-reply"></i>
            </a>
        </div>
        @if(Auth::user()->tipo_user == 1)
        <div class="btn-group">
            <a href="{{url('/ciclo_item/cadastro_item',$id)}}" class=" btn green sbold" > Novo
                <i class="fa fa-plus"></i>
            </a>
        </div>
        @endif
        <div class="btn-group" >
            <a href="{{ url('/ciclo/export',$id)}}" class=" btn blue sbold" > Exportar xls
                <i class="fa fa-file-excel-o"></i>
            </a>
        </div>
        <div class="btn-group" >
            <a href="{{ url('/ciclo/itens/import',$id)}}" class=" btn blue sbold" >  Importar
                <i class="fa fa-file-excel-o"></i>
            </a>
        </div>
    </div>
    <div class="portlet-body">

        {{ Form::open(array('id' => 'form')) }} 
        
        <table class="table table-striped table-bordered table-hover" id="example" >
        <!--<table id="example" class="stripe row-border order-column" cellspacing="0" width="100%">-->
        <!--<table id="example" class="display" cellspacing="0" width="100%">-->            
            
            <thead>
                <tr >
                    <th id="formata" style="width: 15%;"> Colaborador </th>
                    <th id="formata" style="width: 15%;"> CPF/Matricula </th>
                    <th id="formata" style="width: 15%;"> Setor </th>
                    <th id="formata" style="width: 15%;"> Qualificação </th>
                    <th id="formata" style="width: 15%;"> Especialização </th>
                    <th id="formata" style="width: 15%;"> Competência </th>
                    <th id="formata" style="width: 15%;"> Tipo Lideranca </th>
                    @if(Auth::user()->tipo_user == 1)
                    <td class="col-lg-1 searchable" id="formata"> Ação </td>
                    @endif
                </tr>
            </thead>

            <tbody id="odd">
                @foreach($objetos as $objeto)
                <tr class="odd gradeX">
                    <td class="uppercase"> <p class="formata"> {{ $objeto->colaborador->nome}} </p></td>
                    <td class="uppercase"> <p class="formata"> {{ $objeto->colaborador->cpf}} / {{ $objeto->colaborador->matricula}}  </p></td>
                    <td class="uppercase"> <p class="formata"> {{ $objeto->colaborador->setor}} </p></td>
                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoQualificacao as $cicloQualificacao)
                        <p class="formata">
                            {{ $cicloQualificacao->qualificacao->descricao }} |
                        </p>
                        @endforeach
                    </td>
                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoEspecializacao as $cicloEspecializacao)
                        <p class="formata">
                            {{ $cicloEspecializacao->especializacao->descricao }} |
                        </p>
                        @endforeach
                    </td>
                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoCompetencia as $cicloCompetencia)
                        <p class="formata">
                            <a href="#" class="dcontexto">{{ $cicloCompetencia->competencia->descricao }} 
                                <span>
                                    @foreach($cicloCompetencia->competencia->competencias as $competencia)
                                    {{ $competencia->descricao }} ; 
                                    @endforeach
                                </span>
                            </a> |  
                        </p>                        
                        @endforeach
                    </td>
                    <td class="uppercase"> 
                        @foreach($objeto->cicloResultadoTipoLideranca as $cicloTipoLideranca)
                        <p class="formata">
                            <a href="#" class="dcontexto"> {{ $cicloTipoLideranca->tipo_lideranca->descricao }} 
                                <span>
                                    @foreach($cicloTipoLideranca->tipo_lideranca->tipo_liderancas as $tipolideranca)
                                    {{ $tipolideranca->descricao }} ; 
                                    @endforeach
                                </span>
                            </a> |  
                        </p>
                        @endforeach
                    </td>
                    @if(Auth::user()->tipo_user == 1)
                    <td >                            
                        <div class="row" >
                            <div class="col-md-5 col-sm-5" >
                                <a href="{{url('/ciclo_item/edita_item',$objeto->codigo_ciclo_resultado)}}" class="btn btn-icon-only blue tooltips" data-placement="top" data-original-title='Editar'>
                                    <i class="fa fa-edit"></i>
                                </a>
                            </div>                               
                            <div class="col-md-5 col-sm-5">    
                                {{ Form::hidden('codigo_ciclo_resultado', $objeto->codigo_ciclo_resultado, array('id'=>'codigo_ciclo_resultado', 'data-id'=>$objeto->codigo_ciclo_resultado)) }} 
                                {{ Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'btn btn-icon-only red deleteItem tooltips','id' => 'btnDelete', 'data-id'=>$objeto->codigo_ciclo_resultado ,'data-ciclo'=>$objeto->ciclo_codigo,'data-placement'=>"top", 'data-original-title'=>'Excluir'] ) }}
                            </div>
                        </div>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ Form::close() }}

    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection
