$(document).ready(function () {
    $('.deleteItem').on('click', function (e) {
        var datatoken = $('input[name=_token]').val();
        var dataId = $(this).data('id');
        $.confirm({
            title: 'Exclusão!',
            content: 'Tem certeza que deseja remover este registro ?',
            buttons: {
                confirmar: function () {
                    $.ajax({
                        url: 'especializacao/destroy',
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            _token: datatoken,
                            codigo_competencia: dataId
                        },
                        beforeSend: function () {
                            SIS.blockUI({
                                target: $("body")
                            });
                        },
                        complete: function () {
                            SIS.unblockUI("body");
                        },
                        error: function () {
                            console.log('error');
                        },
                        success: function (data) {
                            $("body").load("especializacao");
                        }
                    });
                },
                cancelar: function () {
                    $.alert('Cancelado com sucesso!');
                }
            }
        });
    });
   
});

