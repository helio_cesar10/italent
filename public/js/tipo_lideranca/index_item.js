$(document).ready(function () {
    $('.deleteItem').on('click', function (e) {
        var datatoken = $('input[name=_token]').val();
        var dataId = $(this).data('id');
        var dataTipoLideranca = $(this).data('tipo_lideranca');
        $.confirm({
            title: 'Exclusão!',
            content: 'Tem certeza que deseja remover este registro ?',
            buttons: {
                confirmar: function () {
                    $.ajax({
                        url: 'item_tipo_lideranca/destroy_item',
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            _token: datatoken,
                            codigo_item_tipo_lideranca: dataId,
                            tipo_lideranca_codigo_tipo_lideranca: dataTipoLideranca
                        },
                        beforeSend: function () {
//                            $.blockUI({target: $("body"), css: {backgroundColor: '#f00', color: '#fff'}});
                            SIS.blockUI({
                                target: $("body")
                            });
                        },
                        complete: function () {
                            SIS.unblockUI("body");
                        },
                        error: function () {
                            console.log('error');
                        },
                        success: function (data) {
                            $("body").load("tipo_lideranca/itens/"+dataTipoLideranca);
                        }
                    });
                },
                cancelar: function () {
                    $.alert('Cancelado com sucesso!');
                }
            }
        });
    });

});

