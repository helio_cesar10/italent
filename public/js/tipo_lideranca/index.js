$(document).ready(function () {
    $('.deleteItem').on('click', function (e) {
        var datatoken = $('input[name=_token]').val();
        var dataId = $(this).data('id');
        $.confirm({
            title: 'Exclusão!',
            content: 'Tem certeza que deseja remover este registro ?',
            buttons: {
                confirmar: function () {
                    $.ajax({
                        url: 'tipo_lideranca/destroy',
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            _token: datatoken,
                            codigo_tipo_lideranca: dataId
                        },
                        beforeSend: function () {
                            $.blockUI({ target: $("body"), css: { backgroundColor: '#f00', color: '#fff'} });
//                            SIS.blockUI({
//                                target: $("body")
//                            });
                        },
                        complete: function () {
                            SIS.unblockUI("body");
                        },
                        error: function () {
                            console.log('error');
                        },
                        success: function (data) {
                            $("body").load("tipo_lideranca");
                        }
                    });
                },
                cancelar: function () {
                    $.alert('Cancelado com sucesso!');
                }
            }
        });
    });
   
});

