$(document).ready(function () {
    $('.deleteItem').on('click', function (e) {
        var datatoken = $('input[name=_token]').val();
        var dataId = $(this).data('id');
        $.confirm({
            title: 'Exclusão!',
            content: 'Tem certeza que deseja remover este registro ?',
            buttons: {
                confirmar: function () {
                    $.ajax({
                        url: 'usuarios/destroy',
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            _token: datatoken,
                            id: dataId
                        },
                        beforeSend: function () {
//                            App.blockUI({
//                                target: $("body")
//                            });
                        },
                        complete: function () {
//                            App.unblockUI("body");
                        },
                        error: function () {
                            console.log('error');
                        },
                        success: function (data) {
                            $("body").load("usuarios");
                        }
                    });
                },
                cancelar: function () {
                    $.alert('Cancelado com sucesso!');
                }
            }
        });
    });
   
});

