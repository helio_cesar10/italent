$(document).ready(function () {

// Setup - add a text input to each footer cell
    $('#example thead th').each(function (i) {
        var title = $('#example thead th').eq($(this).index()).text();
        $(this).html('<input type="text" style="width: 116px; margin-left: -13px;" placeholder="' + title + '" data-index="' + i + '" />');
    });

    // DataTable
    var table = $('#example').DataTable({
        scrollX:'800px',
        scrollX: false,
        scrollCollapse: true,
        paging: true,
        fixedColumns: true
    });


    // Filter event handler
    $(table.table().container()).on('keyup', 'thead input', function () {
        table
                .column($(this).data('index'))
                .search(this.value)
                .draw();
    });
    $('.deleteItem').on('click', function (e) {
        var datatoken = $('input[name=_token]').val();
        var dataId = $(this).data('id');
        var dataCiclo = $(this).data('ciclo');
        $.confirm({
            title: 'Exclusão!',
            content: 'Tem certeza que deseja remover este registro ?',
            buttons: {
                confirmar: function () {
                    $.ajax({
                        url: '/ciclo_item/destroy_item',
                        type: 'DELETE',
                        dataType: "JSON",
                        data: {
                            _token: datatoken,
                            codigo_ciclo_resultado: dataId,
                            codigo_ciclo: dataCiclo
                        },
                        beforeSend: function () {
//                            $.blockUI({target: $("body"), css: {backgroundColor: '#f00', color: '#fff'}});
                            SIS.blockUI({
                                target: $("body")
                            });
                        },
                        complete: function () {
                            SIS.unblockUI("body");
                        },
                        error: function () {
                            console.log('error');
                        },
                        success: function (data) {
                            $("body").load("ciclo/itens/" + dataCiclo);
                        }
                    });
                },
                cancelar: function () {
                    $.alert('Cancelado com sucesso!');
                }
            }
        });
    });
});

