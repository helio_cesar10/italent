<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $titulo or 'Sistema HC - Tecnologia' }}</title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-dataTables.css') }}">
	
</head>
    <body>
        @include('templates.menu')
        
        <div class="container">
            @yield('content')
        </div>
        
        <script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>		
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>		
		<script src="{{ asset('assets/js/bootstrap-dataTables-paging.js') }}"></script>		
		<script src="{{ asset('assets/js/jquery.maskMoney.js') }}"></script>	
		<script src="{{ asset('assets/js/funcoes.js') }}"></script>	
		<script src="https://raw.githubusercontent.com/digitalBush/jquery.maskedinput/1.4.0/dist/jquery.maskedinput.js"></script>
		<script>
			jQuery(function($){
				$('input[name="contato"]').mask("(99)9999-9999");
				$("#data").mask("99/99/9999");
			});
		</script>
    </body>
</html>