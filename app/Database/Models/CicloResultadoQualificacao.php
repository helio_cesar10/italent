<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 *
 * @author HELIOCESAR
 */
class CicloResultadoQualificacao extends Model implements AuditableContract {

    use SoftDeletes;

    use Auditable;

    protected $table = 'ciclo_resultado_has_qualificacao';
    public $primaryKey = 'ciclo_resultado_qualificacao';

    public function qualificacao() {
        return $this->belongsTo('App\Database\Models\Qualificacao', 'qualificacao_codigo', 'codigo_qualificacao');
    }

}
