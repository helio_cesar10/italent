<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 *
 * @author HELIOCESAR
 */
class TipoLideranca extends Model implements AuditableContract {

    use SoftDeletes;

use Auditable;

    protected $table = 'tipo_lideranca';
    public $primaryKey = 'codigo_tipo_lideranca';

//    public $sequence = 'seq_capacitacao';


    public function tipo_liderancas() {
        return $this->hasMany('App\Database\Models\ItemTipoLideranca', 'tipo_lideranca_codigo_tipo_lideranca');
    }

//
//    public function termo() {
//        return $this->belongsTo('App\Database\Models\TermoReferencia', 'codigo_termo', 'codigo_termo');
//    }
//
//    public function municipio() {
//        return $this->belongsTo('App\Database\Models\Municipio', 'mun_codigo', 'mun_codigo');
//    }
//
//    public function capacitacaoAreaAtuacoes() {
//        return $this->hasMany('App\Database\Models\CapacitacaoAreaAtuacao', 'codigo_capacitacao');
//    }
//
//    public function turmas() {
//        return $this->hasMany('App\Database\Models\CapacitacaoTurma', 'codigo_capacitacao');
//    }
//
//    public function capacitacaopublicoAlvos() {
//        return $this->hasMany('App\Database\Models\CapacitacaoPublicoAlvo', 'codigo_capacitacao');
//    }
//
//    public function modelo_certificado() {
//        return $this->belongsTo('App\Database\Models\ModeloCertificado', 'codigo_modelo_certificado', 'codigo_modelo_certificado');
//    }
//    
//    public function capacitacao_situacao() {
//        return $this->belongsTo('App\Database\Models\CapacitacaoSituacao', 'codigo_capacitacao_situacao', 'codigo_capacitacao_situacao');
//    }
//
//    public function getDataInicio() {
//        return Carbon::parse($this->attributes['data_inicio'])->format('d-m-Y');
//    }
//
//    public function getDataTermino() {
//        return Carbon::parse($this->attributes['data_termino'])->format('d-m-Y');
//    }
}
