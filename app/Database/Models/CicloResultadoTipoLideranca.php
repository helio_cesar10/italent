<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 *
 * @author HELIOCESAR
 */
class CicloResultadoTipoLideranca extends Model implements AuditableContract {

    use SoftDeletes;

    use Auditable;

    protected $table = 'ciclo_resultado_has_tipo_lideranca';
    public $primaryKey = 'ciclo_resultado_tipo_lideranca';

    public function tipo_lideranca() {
        return $this->belongsTo('App\Database\Models\TipoLideranca', 'tipo_lideranca_codigo', 'codigo_tipo_lideranca');
    }

}
