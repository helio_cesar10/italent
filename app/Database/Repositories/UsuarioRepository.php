<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Usuario;

/**
 *
 * @author HELIOCESAR
 */
class UsuarioRepository {

    public function findAll() {
        $objetos = Usuario::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function findId($data) {
        $objeto = Usuario::where('id', $data)->first();
        return $objeto;
    }

    public function create($data) {
        DB::beginTransaction();
        try {
//            $data['descricao']
            $objeto = new Usuario();
            $objeto->name = Str::upper($data['nome']);
            $objeto->email = Str::upper($data['email']);
            $objeto->password = Str::upper($data['password']);
            $objeto->tipo_user = $data['tipo_usuario'];
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data) {
        $id = $data['id'];
        DB::beginTransaction();
        try {
            $objeto = Usuario::where('id', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->name = Str::upper($data['nome']);
            $objeto->email = Str::upper($data['email']);
            $objeto->password = Str::upper($data['password']);
            $objeto->tipo_user = $data['tipo_usuario'];
            $objeto->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['id'];
        DB::beginTransaction();
        try {
            $objeto = Usuario::where('id', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

}
