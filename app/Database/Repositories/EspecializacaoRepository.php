<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Especializacao;
use App\Database\Models\EspecializacaoAssunto;

/**
 *
 * @author HELIOCESAR
 */
class EspecializacaoRepository {

    public function findAll() {
        $objetos = Especializacao::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function findDescricao($data) {
        $objeto = Especializacao::where('descricao', $data)->first();
        return $objeto;
    }

    public function findAllEspecializacao() {
        $objetos = Especializacao::orderBy('created_at', 'desc')->pluck('descricao', 'codigo_especializacao');
        return $objetos;
    }

    public function findId($data) {
        $especializacao = Especializacao::where('codigo_especializacao', $data)->first();
        return $especializacao;
    }

    public function create($data) {
        DB::beginTransaction();
        try {
            $especializacao = new Especializacao();
            $especializacao->descricao = Str::upper($data['descricao']);
            $especializacao->save();
            DB::commit();
            return $especializacao;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data) {
        $id = $data['codigo_especializacao'];
        DB::beginTransaction();
        try {
            $especializacao = Especializacao::where('codigo_especializacao', $id)->first();
            if (!$especializacao) {
                \App::abort(404);
            }
            $especializacao->descricao = Str::upper($data['descricao']);
            $especializacao->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['codigo_especializacao'];
        DB::beginTransaction();
        try {
            $especializacao = Especializacao::where('codigo_especializacao', $id)->first();
            if (!$especializacao) {
                \App::abort(404);
            }
            $especializacao->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findIdAssunto($data) {
        $objeto = EspecializacaoAssunto::where('codigo_especializacao_assunto', $data)->first();
        return $objeto;
    }

    public function updateAssunto($data) {
        $id = $data['codigo_especializacao_assunto'];
        DB::beginTransaction();
        try {
            $objeto = EspecializacaoAssunto::where('codigo_especializacao_assunto', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy_assunto($data) {
        $id = $data['codigo_especializacao_assunto'];
        DB::beginTransaction();
        try {
            $objeto = EspecializacaoAssunto::where('codigo_especializacao_assunto', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findAllAssuntos($id) {
        $objetos = EspecializacaoAssunto::where('especializacao_codigo_especializacao', $id)->orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function createAssuntos($data) {
        DB::beginTransaction();
        try {
            $objeto = new EspecializacaoAssunto();
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->especializacao_codigo_especializacao = $data['especializacao_codigo_especializacao'];
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

}
