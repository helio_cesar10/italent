<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\TipoLideranca;
use App\Database\Models\ItemTipoLideranca;

/**
 *
 * @author HELIOCESAR
 */
class TipoLiderancaRepository {

    public function findAll() {
        $objetos = TipoLideranca::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function findAllTipoLideranca() {
        $objetos = TipoLideranca::orderBy('created_at', 'desc')->pluck('descricao', 'codigo_tipo_lideranca');
        return $objetos;
    }

    public function findDescricao($data) {
        $objeto = TipoLideranca::where('descricao', $data)->first();
        return $objeto;
    }

    public function findId($data) {
        $objeto = TipoLideranca::where('codigo_tipo_lideranca', $data)->first();
        return $objeto;
    }

    public function create($data) {
        DB::beginTransaction();
        try {
//            $data['descricao']
            $objeto = new TipoLideranca();
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data) {
        $id = $data['codigo_tipo_lideranca'];
        DB::beginTransaction();
        try {
            $objeto = TipoLideranca::where('codigo_tipo_lideranca', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['codigo_tipo_lideranca'];
        DB::beginTransaction();
        try {
            $objeto = TipoLideranca::where('codigo_tipo_lideranca', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findIdItem($data) {
        $objeto = ItemTipoLideranca::where('codigo_item_tipo_lideranca', $data)->first();
        return $objeto;
    }

    public function updateItem($data) {
        $id = $data['codigo_item_tipo_lideranca'];
        DB::beginTransaction();
        try {
            $objeto = ItemTipoLideranca::where('codigo_item_tipo_lideranca', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy_item($data) {
        $id = $data['codigo_item_tipo_lideranca'];
        DB::beginTransaction();
        try {
            $objeto = ItemTipoLideranca::where('codigo_item_tipo_lideranca', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findAllItems($id) {
        $objetos = ItemTipoLideranca::where('tipo_lideranca_codigo_tipo_lideranca', $id)->orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function createItem($data) {
        DB::beginTransaction();
        try {
            $objeto = new ItemTipoLideranca();
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->tipo_lideranca_codigo_tipo_lideranca = $data['tipo_lideranca_codigo_tipo_lideranca'];
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

}
