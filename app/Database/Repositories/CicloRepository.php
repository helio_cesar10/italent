<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Ciclo;
use App\Database\Models\CicloResultado;
use App\Database\Models\Pessoas;
use App\Database\Models\Competencia;
use App\Database\Models\CicloResultadoCompetencia;
use App\Database\Models\CicloResultadoEspecializacao;
use App\Database\Models\CicloResultadoQualificacao;
use App\Database\Models\CicloResultadoTipoLideranca;
use Illuminate\Support\Collection;
use Datatables;

/**
 *
 * @author HELIOCESAR
 */
class CicloRepository {

    public function findAll() {
        $objetos = Ciclo::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function findAllColaboradores() {
        $objetos = Pessoas::orderBy('created_at', 'desc')->pluck('nome', 'codigo_pessoas');
        return $objetos;
    }

    public function findId($data) {
        $competencia = Ciclo::where('codigo_ciclo', $data)->first();
        return $competencia;
    }

    public function create($data) {
        DB::beginTransaction();
        try {
            $objeto = new Ciclo();
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data) {
        $id = $data['codigo_ciclo'];
        DB::beginTransaction();
        try {
            $objeto = Ciclo::where('codigo_ciclo', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['codigo_ciclo'];
        DB::beginTransaction();
        try {
            $objeto = Ciclo::where('codigo_ciclo', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findIdItem($data) {
        $objeto = CicloResultado::where('codigo_ciclo_resultado', $data)->first();
        return $objeto;
    }

    public function findIdItemPessoa($data) {
        $objeto = CicloResultado::where('pessoas_codigo', $data)->first();
        return $objeto;
    }

    public function findCicloResultadoCompetencia($ciclo_resultado_codigo, $competencia_codigo) {
        $objeto = CicloResultadoCompetencia::where('ciclo_resultado_codigo', $ciclo_resultado_codigo)
                ->where('competencia_codigo', $competencia_codigo)
                ->first();
        return $objeto;
    }

    public function findCicloResultadoEspecializacao($ciclo_resultado_codigo, $especializacao_codigo) {
        $objeto = CicloResultadoEspecializacao::where('ciclo_resultado_codigo', $ciclo_resultado_codigo)
                ->where('especializacao_codigo', $especializacao_codigo)
                ->first();
        return $objeto;
    }

    public function findCicloResultadoTipoLideranca($ciclo_resultado_codigo, $tipo_lideranca_codigo) {
        $objeto = CicloResultadoTipoLideranca::where('ciclo_resultado_codigo', $ciclo_resultado_codigo)
                ->where('tipo_lideranca_codigo', $tipo_lideranca_codigo)
                ->first();
        return $objeto;
    }

    public function findCicloResultadoQualificacao($ciclo_resultado_codigo, $qualificacao_codigo) {
        $objeto = CicloResultadoQualificacao::where('ciclo_resultado_codigo', $ciclo_resultado_codigo)
                ->where('qualificacao_codigo', $qualificacao_codigo)
                ->first();
        return $objeto;
    }

    public function destroy_item($data) {
        $id = $data['codigo_ciclo_resultado'];
        DB::beginTransaction();
        try {
            $objeto = CicloResultado::where('codigo_ciclo_resultado', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findAllItems($id) {

        $objetos = CicloResultado::where('ciclo_codigo', $id)->orderBy('created_at', 'desc')->get();
//        $objetos = \DB::table('ciclo_resultado')
//        $objetos = CicloResultado::join('pessoas', 'ciclo_resultado.pessoas_codigo', '=', 'pessoas.codigo_pessoas')
//                        ->where('ciclo_codigo', $id)->get();
//        return Datatables::of($objetos)->make(true);


        return $objetos;
    }

//    public function findAllItemsId($id) {
//        $objetos = CicloResultado::where('codigo_ciclo_resultado', $id)->first();
//
//        return $objetos;
//    }

    public function findAllItemsId($valores) {
        $competencias = '';
        $qualificacao = '';
        $perfil = '';
        $tipo_lideranca = '';
        $where = '';
        if ($valores->competencias != 0) {
            $where .= " and ciclo_resultado_has_competencia.competencia_codigo = " . $valores->competencias;
        };
        if ($valores->qualificacao != 0) {
            $where .= " and ciclo_resultado_has_qualificacao.qualificacao_codigo = " . $valores->qualificacao;
        };
        if ($valores->perfil != 0) {
            $where .= " and ciclo_resultado_has_especializacao.especializacao_codigo = " . $valores->perfil;
        };
        if ($valores->tipo_lideranca != 0) {
            $where .= " and ciclo_resultado_has_tipo_lideranca.tipo_lideranca_codigo = " . $valores->tipo_lideranca;
        };
        if ($valores->competencias != 0) {
            $competencias = $valores->competencias;
        };
        if ($valores->qualificacao != 0) {
            $qualificacao = $valores->qualificacao;
        };
        if ($valores->perfil != 0) {
            $perfil = $valores->perfil;
        };
        if ($valores->tipo_lideranca != 0) {
            $tipo_lideranca = $valores->tipo_lideranca;
        };
//        $objetos = CicloResultado::join('pessoas', 'ciclo_resultado.pessoas_codigo', '=', 'pessoas.codigo_pessoas')
//                ->Join('ciclo_resultado_has_competencia', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_competencia.ciclo_resultado_codigo')
//                ->join('competencia', 'ciclo_resultado_has_competencia.competencia_codigo', '=', 'competencia.codigo_competencia')
//                ->Join('ciclo_resultado_has_especializacao', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_especializacao.ciclo_resultado_codigo')
//                ->join('especializacao', 'ciclo_resultado_has_especializacao.especializacao_codigo', '=', 'especializacao.codigo_especializacao')
//                ->Join('ciclo_resultado_has_qualificacao', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_qualificacao.ciclo_resultado_codigo')
//                ->join('qualificacao', 'ciclo_resultado_has_qualificacao.qualificacao_codigo', '=', 'qualificacao.codigo_qualificacao')
//                ->Join('ciclo_resultado_has_tipo_lideranca', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_tipo_lideranca.ciclo_resultado_codigo')
//                ->join('tipo_lideranca', 'ciclo_resultado_has_tipo_lideranca.tipo_lideranca_codigo', '=', 'tipo_lideranca.codigo_tipo_lideranca')
//                ->Where('ciclo_resultado_has_competencia.competencia_codigo', $competencias)
//                ->Where('ciclo_resultado_has_qualificacao.qualificacao_codigo', $qualificacao)
//                ->Where('ciclo_resultado_has_especializacao.especializacao_codigo', $perfil)
//                ->Where('ciclo_resultado_has_tipo_lideranca.tipo_lideranca_codigo', $tipo_lideranca)
//                ->groupBy('pessoas.codigo_pessoas','')
//                ->get();
//        $collectionion = new Collection();
//
        $objetos = DB::select(" 
        select * from `ciclo_resultado` 
        inner join `pessoas` on `ciclo_resultado`.`pessoas_codigo` = `pessoas`.`codigo_pessoas` 
        inner join `ciclo_resultado_has_competencia` on `ciclo_resultado`.`codigo_ciclo_resultado` = `ciclo_resultado_has_competencia`.`ciclo_resultado_codigo` 
        inner join `competencia` on `ciclo_resultado_has_competencia`.`competencia_codigo` = `competencia`.`codigo_competencia` 
        inner join `ciclo_resultado_has_especializacao` on `ciclo_resultado`.`codigo_ciclo_resultado` = `ciclo_resultado_has_especializacao`.`ciclo_resultado_codigo` 
        inner join `especializacao` on `ciclo_resultado_has_especializacao`.`especializacao_codigo` = `especializacao`.`codigo_especializacao` 
        inner join `ciclo_resultado_has_qualificacao` on `ciclo_resultado`.`codigo_ciclo_resultado` = `ciclo_resultado_has_qualificacao`.`ciclo_resultado_codigo` 
        inner join `qualificacao` on `ciclo_resultado_has_qualificacao`.`qualificacao_codigo` = `qualificacao`.`codigo_qualificacao` 
        inner join `ciclo_resultado_has_tipo_lideranca` on `ciclo_resultado`.`codigo_ciclo_resultado` = `ciclo_resultado_has_tipo_lideranca`.`ciclo_resultado_codigo` 
        inner join `tipo_lideranca` on `ciclo_resultado_has_tipo_lideranca`.`tipo_lideranca_codigo` = `tipo_lideranca`.`codigo_tipo_lideranca` 
        where `ciclo_resultado`.`deleted_at` is null
        " . $where);
        $ids = array();
        if (!empty($objetos)) {

            foreach ($objetos as $value) {
                $ids[] = $value->codigo_ciclo_resultado;
            }
            array_unique($ids);
        }
        $objetos = CicloResultado::whereIn('codigo_ciclo_resultado', $ids)->get();
        return $objetos;
    }

    public function findAllItemsExport($id) {

//        $objetos = CicloResultado::where('ciclo_codigo', $id)->orderBy('created_at', 'desc')->get();
//        $objetos = \DB::table('ciclo_resultado')
        $objetos = CicloResultado::
                join('pessoas', 'ciclo_resultado.pessoas_codigo', '=', 'pessoas.codigo_pessoas')
                ->join('ciclo_resultado_has_competencia', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_competencia.ciclo_resultado_codigo')
                ->join('competencia', 'ciclo_resultado_has_competencia.competencia_codigo', '=', 'competencia.codigo_competencia')
                ->join('ciclo_resultado_has_especializacao', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_especializacao.ciclo_resultado_codigo')
                ->join('especializacao', 'ciclo_resultado_has_especializacao.especializacao_codigo', '=', 'especializacao.codigo_especializacao')
                ->join('ciclo_resultado_has_qualificacao', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_qualificacao.ciclo_resultado_codigo')
                ->join('qualificacao', 'ciclo_resultado_has_qualificacao.qualificacao_codigo', '=', 'qualificacao.codigo_qualificacao')
                ->join('ciclo_resultado_has_tipo_lideranca', 'ciclo_resultado.codigo_ciclo_resultado', '=', 'ciclo_resultado_has_tipo_lideranca.ciclo_resultado_codigo')
                ->join('tipo_lideranca', 'ciclo_resultado_has_tipo_lideranca.tipo_lideranca_codigo', '=', 'tipo_lideranca.codigo_tipo_lideranca')
                ->where('ciclo_codigo', $id)
                ->select('pessoas.nome', 'pessoas.cpf', 'pessoas.matricula', 'pessoas.setor', 'competencia.descricao as competencia', 'especializacao.descricao as especializacao', 'qualificacao.descricao as qualificacao', 'tipo_lideranca.descricao as tipo_lideranca'
                )
                ->get();
//        return Datatables::of($objetos)->make(true);


        return $objetos;
    }

    public function findAllItemsExportTalento($id) {
        $objeto = CicloResultado::where('codigo_ciclo_resultado', $id)->first();
        return $objeto;
    }

    public function createItem($data) {
        DB::beginTransaction();
        try {
            $objeto = $this->findIdItemPessoa($data['colaborador']);
            if (empty($objeto)) {
                $objeto = new CicloResultado();
                $objeto->pessoas_codigo = $data['colaborador'];
                $objeto->ciclo_codigo = $data['ciclo_codigo'];
                $objeto->save();
            }
            if (!empty($data["competencias"])) {
                $competencias = $data["competencias"];
                foreach ($competencias as $value) {
                    $cicloResultadoCompetencia = $this->findCicloResultadoCompetencia($objeto->codigo_ciclo_resultado, $value);
                    if (empty($cicloResultadoCompetencia)) {
                        $competencia = new CicloResultadoCompetencia();
                        $competencia->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $competencia->competencia_codigo = $value;
                        $competencia->save();
                    }
                }
            }

            if (!empty($data["especializacao"])) {
                $especializacoes = $data["especializacao"];
                foreach ($especializacoes as $value) {
                    $cicloResultadoEspecializacao = $this->findCicloResultadoEspecializacao($objeto->codigo_ciclo_resultado, $value);
                    if (empty($cicloResultadoEspecializacao)) {
                        $especializacao = new CicloResultadoEspecializacao();
                        $especializacao->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $especializacao->especializacao_codigo = $value;
                        $especializacao->save();
                    }
                }
            }

            if (!empty($data["qualificacao"])) {
                $qualificacoes = $data["qualificacao"];
                foreach ($qualificacoes as $value) {
                    $cicloResultadoQualificacao = $this->findCicloResultadoQualificacao($objeto->codigo_ciclo_resultado, $value);
                    if (empty($cicloResultadoQualificacao)) {
                        $qualificacao = new CicloResultadoQualificacao();
                        $qualificacao->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $qualificacao->qualificacao_codigo = $value;
                        $qualificacao->save();
                    }
                }
            }

            if (!empty($data["tipo_lideranca"])) {
                $tipo_liderancas = $data["tipo_lideranca"];
                foreach ($tipo_liderancas as $value) {
                    $cicloResultadoTipoLideranca = $this->findCicloResultadoTipoLideranca($objeto->codigo_ciclo_resultado, $value);
                    if (empty($cicloResultadoTipoLideranca)) {
                        $tipo_lideranca = new CicloResultadoTipoLideranca();
                        $tipo_lideranca->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $tipo_lideranca->tipo_lideranca_codigo = $value;
                        $tipo_lideranca->save();
                    }
                }
            }

            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function updateItem($data) {
        $id = $data['codigo_ciclo_resultado'];
        DB::beginTransaction();
        try {
            $objeto = CicloResultado::where('codigo_ciclo_resultado', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->pessoas_codigo = $data['colaborador'];
            $objeto->save();
////Ediacao de Competencia
            if (0 < count($data["competencias"])) {
                for ($i = 0; $i < count($data["competencias"]); $i++) {
                    $competencia = CicloResultadoCompetencia::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->where('competencia_codigo', $data["competencias"][$i])->first();

                    if (empty($competencia)) {
                        $competencia = new CicloResultadoCompetencia();
                        $competencia->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $competencia->competencia_codigo = $data["competencias"][$i];
                        $competencia->save();
                    } else {
                        $competencias = CicloResultadoCompetencia::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();

                        foreach ($competencias as $compete) {
                            if (!in_array($compete->competencia_codigo, $data["competencias"])) {
                                $compete->delete();
                            }
                        }
                    }
                }
            } else {
                $competencias = CicloResultadoCompetencia::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();
                foreach ($competencias as $compete) {
                    $compete->delete();
                }
            }
////Ediacao de Especializacao
            if (0 < count($data["especializacao"])) {
                for ($i = 0; $i < count($data["especializacao"]); $i++) {
                    $especializacao = CicloResultadoEspecializacao::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->where('especializacao_codigo', $data["especializacao"][$i])->first();

                    if (empty($especializacao)) {
                        $especializacao = new CicloResultadoEspecializacao();
                        $especializacao->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $especializacao->especializacao_codigo = $data["especializacao"][$i];
                        $especializacao->save();
                    } else {
                        $especializacoes = CicloResultadoEspecializacao::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();

                        foreach ($especializacoes as $especializa) {
                            if (!in_array($especializa->especializacao_codigo, $data["especializacao"])) {
                                $especializa->delete();
                            }
                        }
                    }
                }
            } else {
                $especializacoes = CicloResultadoEspecializacao::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();
                foreach ($especializacoes as $especializa) {
                    $especializa->delete();
                }
            }

////Ediacao de Qualificacao
            if (0 < count($data["qualificacao"])) {
                for ($i = 0; $i < count($data["qualificacao"]); $i++) {
                    $qualificacao = CicloResultadoQualificacao::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->where('qualificacao_codigo', $data["qualificacao"][$i])->first();

                    if (empty($qualificacao)) {
                        $qualificacao = new CicloResultadoQualificacao();
                        $qualificacao->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $qualificacao->qualificacao_codigo = $data["qualificacao"][$i];
                        $qualificacao->save();
                    } else {
                        $qualificacoes = CicloResultadoQualificacao::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();

                        foreach ($qualificacoes as $qualifica) {
                            if (!in_array($qualifica->qualificacao_codigo, $data["qualificacao"])) {
                                $qualifica->delete();
                            }
                        }
                    }
                }
            } else {
                $qualificacoes = CicloResultadoQualificacao::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();
                foreach ($qualificacoes as $qualifica) {
                    $qualifica->delete();
                }
            }

////Ediacao de Tipo Lideranca
            if (0 < count($data["tipo_lideranca"])) {
                for ($i = 0; $i < count($data["tipo_lideranca"]); $i++) {
                    $tipo_lideranca = CicloResultadoTipoLideranca::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->where('tipo_lideranca_codigo', $data["tipo_lideranca"][$i])->first();

                    if (empty($tipo_lideranca)) {
                        $tipo_lideranca = new CicloResultadoTipoLideranca();
                        $tipo_lideranca->ciclo_resultado_codigo = $objeto->codigo_ciclo_resultado;
                        $tipo_lideranca->tipo_lideranca_codigo = $data["tipo_lideranca"][$i];
                        $tipo_lideranca->save();
                    } else {
                        $tipo_liderancoes = CicloResultadoTipoLideranca::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();

                        foreach ($tipo_liderancoes as $tipo_lidera) {
                            if (!in_array($tipo_lidera->tipo_lideranca_codigo, $data["tipo_lideranca"])) {
                                $tipo_lidera->delete();
                            }
                        }
                    }
                }
            } else {
                $tipo_liderancoes = CicloResultadoTipoLideranca::where('ciclo_resultado_codigo', $objeto->codigo_ciclo_resultado)->get();
                foreach ($tipo_liderancoes as $tipo_lidera) {
                    $tipo_lidera->delete();
                }
            }

            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function cicloResultadoCompetencia($id) {
        $objetos = CicloResultadoCompetencia::where('competencia_codigo', $id)->get();
        return $objetos;
    }

    public function cicloResultadoQualificacao($id) {
        $objetos = CicloResultadoQualificacao::where('qualificacao_codigo', $id)->get();
        return $objetos;
    }

    public function cicloResultadoEspecializacao($id) {
        $objetos = CicloResultadoEspecializacao::where('especializacao_codigo', $id)->get();
        return $objetos;
    }

    public function cicloResultadoTipoLideranca($id) {
        $objetos = CicloResultadoTipoLideranca::where('tipo_lideranca_codigo', $id)->get();
        return $objetos;
    }

//    
//    public function create($data) {
//        DB::beginTransaction();
//        try {
//            $capacitacao = new Capacitacao();
//            $capacitacao->titulo_capacitacao = Str::upper($data['titulo_capacitacao']);
//            $capacitacao->codigo_termo = $data['numero_termo'];
//            $capacitacao->mun_codigo = $data['municipio'];
//            $capacitacao->codigo_solicitante = $data['unidade_solicitante'];
//            $capacitacao->fiscalizados = $data['exclusivo_fiscalizado'];
//            $capacitacao->aula_sabado = $data['sabado'];
//            $capacitacao->aula_domingo = $data['domingo'];
//            $capacitacao->coordenador = Str::upper($data['coordenador']);
//            $capacitacao->responsavel = Str::upper($data['responsavel']);
//            $capacitacao->frequencia_minima = $data['frequencia'];
//            $capacitacao->codigo_capacitacao_situacao = $data['situacao'];
//            $capacitacao->codigo_tipo_area = $data['tipo_area'];
//            $capacitacao->codigo_tipo_capacitacao = $data['tipo_capacitacao'];
//            $capacitacao->codigo_capacitacao_especie = $data['especie'];
//            $capacitacao->data_inicio = Carbon::parse($data['data_inicio']);
//            $capacitacao->data_termino = Carbon::parse($data['data_termino']);
//            $capacitacao->carga_horaria = $data['carga_horaria'];
//            $capacitacao->numero_vaga = $data['vaga'];
//            $capacitacao->descricao = Str::upper($data['descricao']);
//            $capacitacao->avaliacao = $data['exigir_avaliacao'];
//
//            ///certificado
//            $capacitacao->texto_certificado_primeiro = Str::upper($data['primeira_linha']);
//            $capacitacao->texto_certificado_corpo_frente = Str::upper($data['corpo']);
//            $capacitacao->texto_certificado_verso = Str::upper($data['verso']);
//            $capacitacao->codigo_modelo_certificado = $data['modelo_certificado'];
//            ///estrutura-curricular
//            $capacitacao->estrutura_curricular = $data['estrutura_curricular'];
//
//            $capacitacao->save();
//
//            for ($i = 0; $i < count($data["area_atuacoes"]); $i++) {
//                $capacitacaoAreaAtuacao = new CapacitacaoAreaAtuacao();
//                $capacitacaoAreaAtuacao->codigo_capacitacao = $capacitacao->codigo_capacitacao;
//                $capacitacaoAreaAtuacao->codigo_atuacao = $data["area_atuacoes"][$i];
//                $capacitacaoAreaAtuacao->save();
//            }
//            for ($i = 0; $i < count($data["publico_alvos"]); $i++) {
//                $capacitacaoPublicoAlvo = new CapacitacaoPublicoAlvo();
//                $capacitacaoPublicoAlvo->codigo_capacitacao = $capacitacao->codigo_capacitacao;
//                $capacitacaoPublicoAlvo->codigo_publico_alvo = $data["publico_alvos"][$i];
//                $capacitacaoPublicoAlvo->save();
//            }
//            DB::commit();
//            return $capacitacao;
//        } catch (Exception $ex) {
//            DB::rollback();
//            throw new Exception($ex->getMessage());
//        }
//    }
//
//    public function update($data) {
//        $id = $data['codigo_capacitacao'];
//        DB::beginTransaction();
//        try {
//            $capacitacao = Capacitacao::where('codigo_capacitacao', $id)->first();
//            if (!$capacitacao) {
//                App::abort(404);
//            }
//            $capacitacao->titulo_capacitacao = Str::upper($data['titulo_capacitacao']);
//            $capacitacao->codigo_termo = $data['numero_termo'];
//            $capacitacao->mun_codigo = $data['municipio'];
//            $capacitacao->codigo_solicitante = $data['unidade_solicitante'];
//            $capacitacao->fiscalizados = $data['exclusivo_fiscalizado'];
//            $capacitacao->aula_sabado = $data['sabado'];
//            $capacitacao->aula_domingo = $data['domingo'];
//            $capacitacao->coordenador = Str::upper($data['coordenador']);
//            $capacitacao->responsavel = Str::upper($data['responsavel']);
//            $capacitacao->frequencia_minima = $data['frequencia'];
//            $capacitacao->codigo_capacitacao_situacao = $data['situacao'];
//            $capacitacao->codigo_tipo_area = $data['tipo_area'];
//            $capacitacao->codigo_tipo_capacitacao = $data['tipo_capacitacao'];
//            $capacitacao->codigo_capacitacao_especie = $data['especie'];
//            $capacitacao->data_inicio = Carbon::parse($data['data_inicio']);
//            $capacitacao->data_termino = Carbon::parse($data['data_termino']);
//            $capacitacao->carga_horaria = $data['carga_horaria'];
//            $capacitacao->numero_vaga = $data['vaga'];
//            $capacitacao->descricao = Str::upper($data['descricao']);
//            $capacitacao->avaliacao = $data['exigir_avaliacao'];
//
//            ///certificado
//            $capacitacao->texto_certificado_primeiro = Str::upper($data['primeira_linha']);
//            $capacitacao->texto_certificado_corpo_frente = Str::upper($data['corpo']);
//            $capacitacao->texto_certificado_verso = Str::upper($data['verso']);
//            $capacitacao->codigo_modelo_certificado = $data['modelo_certificado'];
//            ///estrutura-curricular
//            $capacitacao->estrutura_curricular = $data['estrutura_curricular'];
//
//            $capacitacao->save();
//            ////Ediacao de Publico alvo
//            if (0 < count($data["publico_alvos"])) {
//                for ($i = 0; $i < count($data["publico_alvos"]); $i++) {
//                    $publicoAlvo = CapacitacaoPublicoAlvo::where('codigo_capacitacao', $capacitacao->codigo_capacitacao)->where('codigo_publico_alvo', $data["publico_alvos"][$i])->first();
//                    if (empty($publicoAlvo)) {
//                        $publicoAlvo = new CapacitacaoPublicoAlvo();
//                        $publicoAlvo->codigo_capacitacao = $capacitacao->codigo_capacitacao;
//                        $publicoAlvo->codigo_publico_alvo = $data["publico_alvos"][$i];
//                        $publicoAlvo->save();
//                    } else {
//                        $publicoAlvos = CapacitacaoPublicoAlvo::where('codigo_capacitacao', $capacitacao->codigo_capacitacao)->get();
//
//                        foreach ($publicoAlvos as $publicoAlvo) {
//                            if (!in_array($publicoAlvo->codigo_publico_alvo, $data["publico_alvos"])) {
//                                $publicoAlvo->delete();
//                            }
//                        }
//                    }
//                }
//            } else {
//                $publicoAlvos = CapacitacaoPublicoAlvo::where('codigo_capacitacao', $capacitacao->codigo_capacitacao)->get();
//                foreach ($publicoAlvos as $publicoAlvo) {
//                    $publicoAlvo->delete();
//                }
//            }
//
//            ////Ediacao de Area de Atuacao
//            if (0 < count($data["area_atuacoes"])) {
//                for ($i = 0; $i < count($data["area_atuacoes"]); $i++) {
//                    $areaAtuacao = CapacitacaoAreaAtuacao::where('codigo_capacitacao', $capacitacao->codigo_capacitacao)->where('codigo_atuacao', $data["area_atuacoes"][$i])->first();
//                    if (empty($areaAtuacao)) {
//                        $areaAtuacao = new CapacitacaoAreaAtuacao();
//                        $areaAtuacao->codigo_capacitacao = $capacitacao->codigo_capacitacao;
//                        $areaAtuacao->codigo_atuacao = $data["area_atuacoes"][$i];
//                        $areaAtuacao->save();
//                    } else {
//                        $areaAtuacoes = CapacitacaoAreaAtuacao::where('codigo_capacitacao', $capacitacao->codigo_capacitacao)->get();
//
//                        foreach ($areaAtuacoes as $areaAtuacao) {
//                            if (!in_array($areaAtuacao->codigo_atuacao, $data["area_atuacoes"])) {
//                                $areaAtuacao->delete();
//                            }
//                        }
//                    }
//                }
//            } else {
//                $areaAtuacoes = CapacitacaoAreaAtuacao::where('codigo_capacitacao', $capacitacao->codigo_capacitacao)->get();
//                foreach ($areaAtuacoes as $areaAtuacao) {
//                    $areaAtuacao->delete();
//                }
//            }
//
//            DB::commit();
//        } catch (Exception $ex) {
//            DB::rollback();
//            throw new Exception($ex->getMessage());
//        }
//    }
}
