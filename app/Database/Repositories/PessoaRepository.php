<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Pessoas;

/**
 *
 * @author HELIOCESAR
 */
class PessoaRepository {

    public function findAll() {
        $objetos = Pessoas::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function findCpf($data) {
        $pessoa = Pessoas::where('cpf', $data)->first();
        return $pessoa;
    }

    public function findId($data) {
        $objeto = Pessoas::where('codigo_pessoas', $data)->first();
        return $objeto;
    }

    public function create($data) {
        
        DB::beginTransaction();
        try {
//            $data['descricao']
            $objeto = new Pessoas();
            $objeto->nome = Str::upper($data['nome']);
            $objeto->cpf = $data['cpf'];
            $objeto->matricula = $data['matricula'];
            $objeto->setor = $data['setor'];
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data) {
        $id = $data['codigo_pessoas'];
        DB::beginTransaction();
        try {
            $objeto = Pessoas::where('codigo_pessoas', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->nome = Str::upper($data['nome']);
            $objeto->cpf = $data['cpf'];
            $objeto->matricula = $data['matricula'];
            $objeto->setor = Str::upper($data['setor']);
            $objeto->save();
            $objeto->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['codigo_pessoas'];
        DB::beginTransaction();
        try {
            $objeto = Pessoas::where('codigo_pessoas', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

}
