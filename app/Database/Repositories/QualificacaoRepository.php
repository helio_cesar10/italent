<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Qualificacao;

/**
 *
 * @author HELIOCESAR
 */
class QualificacaoRepository {

    public function findAll() {
        $objetos = Qualificacao::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function findDescricao($data) {
        $objeto = Qualificacao::where('descricao', $data)->first();
        return $objeto;
    }

    public function findAllQualificacao() {
        $objetos = Qualificacao::orderBy('descricao', 'desc')->pluck('descricao', 'codigo_qualificacao');
        return $objetos;
    }

    public function findId($data) {
        $objeto = Qualificacao::where('codigo_qualificacao', $data)->first();
        return $objeto;
    }

    public function create($data) {
        DB::beginTransaction();
        try {
//            $data['descricao']
            $objeto = new Qualificacao();
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data) {
        $id = $data['codigo_qualificacao'];
        DB::beginTransaction();
        try {
            $objeto = Qualificacao::where('codigo_qualificacao', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['codigo_qualificacao'];
        DB::beginTransaction();
        try {
            $objeto = Qualificacao::where('codigo_qualificacao', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

}
