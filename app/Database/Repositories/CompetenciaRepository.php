<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Competencia;
use App\Database\Models\ItemCompetencia;

/**
 *
 * @author HELIOCESAR
 */
class CompetenciaRepository {

    public function findAll() {
        $objetos = Competencia::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function findAllCompetencia() {
        $objetos = Competencia::orderBy('created_at', 'desc')->pluck('descricao', 'codigo_competencia');
        return $objetos;
    }

    public function capacitacoesConcluidas() {
        $capacitacoes = Capacitacao::orderBy('created_at', 'desc')->where('codigo_capacitacao_situacao', 3)->get();
        return $capacitacoes;
    }

    public function findId($data) {
        $competencia = Competencia::where('codigo_competencia', $data)->first();
        return $competencia;
    }

    public function findDescricao($data) {
        $competencia = Competencia::where('descricao', $data)->first();
        return $competencia;
    }

    public function create($data) {
        DB::beginTransaction();
        try {
            $competencia = new Competencia();
            $competencia->descricao = Str::upper($data['descricao']);
            $competencia->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
        return $competencia;
    }

    public function update($data) {
        $id = $data['codigo_competencia'];
        DB::beginTransaction();
        try {
            $competencia = Competencia::where('codigo_competencia', $id)->first();
            if (!$competencia) {
                \App::abort(404);
            }
            $competencia->descricao = Str::upper($data['descricao']);
            $competencia->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['codigo_competencia'];
        DB::beginTransaction();
        try {
            $competencia = Competencia::where('codigo_competencia', $id)->first();
            if (!$competencia) {
                \App::abort(404);
            }
            $competencia->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findIdItem($data) {
        $objeto = ItemCompetencia::where('codigo_iten_competencia', $data)->first();
        return $objeto;
    }

    public function updateItem($data) {
        $id = $data['codigo_iten_competencia'];
        DB::beginTransaction();
        try {
            $objeto = ItemCompetencia::where('codigo_iten_competencia', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy_item($data) {
        $id = $data['codigo_iten_competencia'];
        DB::beginTransaction();
        try {
            $objeto = ItemCompetencia::where('codigo_iten_competencia', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function findAllItems($id) {
        $objetos = ItemCompetencia::where('competencia_codigo_competencia', $id)->orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function createItem($data) {
        DB::beginTransaction();
        try {
            $objeto = new ItemCompetencia();
            $objeto->descricao = Str::upper($data['descricao']);
            $objeto->competencia_codigo_competencia = $data['competencia_codigo_competencia'];
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

}
