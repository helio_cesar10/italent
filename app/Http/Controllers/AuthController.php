<?php

namespace App\Http\Controllers;

use Auth,
    Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Session;
use App\Database\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

class AuthController extends Controller {

//    protected $username = 'logon';
//
//    public function login() {
//        return view('login');
//    }
//
//    public function logout() {
//        Session::flush();
//        return redirect('/');
//    }

    public function login() {

        if (auth()->guard('web')->check()) {
            return redirect('/');
        }
        return view('login');
    }


    public function authenticate(Request $request) {
        try {

            $input = Input::all();
            $message = [
                'required' => 'Senha e Login são obrigatórios'
            ];

            $this->validate($request, ['email' => 'required'], $message);


            if (count($input) > 0) {
                $auth = auth()->guard('web');
                $credentials = [
                    'email' => $input['email'],
                    'password' => $input['password'],
                ];
                $user = Usuario::where('email', Input::get('email'))->where('password', Input::get('password'))->first();
                if ($user) {
                    $auth->login($user);
                    $auth->attempt($credentials);
                    return redirect('/');
                } else {
                    return \Redirect::route('login');
                }
            } else {
                return \Redirect::route('login');
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function logout() {
        Session::flush();
        auth()->guard('web')->logout();

        return redirect('/');
    }

}
