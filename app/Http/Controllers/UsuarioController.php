<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Database\Repositories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UsuarioController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    private $repository;

    public function __construct(Repositories\UsuarioRepository $repository) {
        $this->repository = $repository;
    }

    public function index() {
        $objetos = $this->repository->findAll();
        return view('usuario.index', ['objetos' => $objetos]);
    }

    public function cadastro() {
        return view('usuario.cadastro');
    }

    public function edita($id) {
        $objeto = $this->repository->findId($id);
        return view('usuario.edita', ['objeto' => $objeto]);
    }

    public function create(Request $request) {
//        $message = ['required' => ':attribute é obrigatório'];
//        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $id = $this->repository->create($request->all());
            return redirect('usuarios');
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update(Request $request) {
        try {
            $this->repository->update($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect('usuarios');
    }

    public function destroy(Request $request) {
        try {
            $this->repository->destroy($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
//        return Response::json();
    }

}
