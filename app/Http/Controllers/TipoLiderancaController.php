<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Database\Repositories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TipoLiderancaController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    private $repository;

    public function __construct(Repositories\TipoLiderancaRepository $repository) {
        $this->repository = $repository;
    }

    public function index() {
        $objetos = $this->repository->findAll();
        return view('tipo_lideranca.index', ['objetos' => $objetos]);
    }

    public function cadastro() {
        return view('tipo_lideranca.cadastro');
    }

    public function edita($id) {
        $objeto = $this->repository->findId($id);
        return view('tipo_lideranca.edita', ['objeto' => $objeto]);
    }

    public function create(Request $request) {
        $message = ['required' => ':attribute é obrigatório'];
        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $id = $this->repository->create($request->all());
            return redirect('tipo_lideranca');
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update(Request $request) {
        try {
            $this->repository->update($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect('tipo_lideranca');
    }

    public function destroy(Request $request) {
        try {
            $this->repository->destroy($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
    }

    public function itens($id) {
        $objetos = $this->repository->findAllItems($id);
        return view('tipo_lideranca.itens', ['id' => $id, 'objetos' => $objetos]);
    }

    public function cadastro_item($id) {
        return view('item_tipo_lideranca.cadastro_item', ['id' => $id]);
    }

    public function edita_item($id) {
        $objeto = $this->repository->findIdItem($id);
        return view('item_tipo_lideranca.edita_item', ['objeto' => $objeto]);
    }

    public function create_item(Request $request) {
        $message = ['required' => ':attribute é obrigatório'];
        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $objeto = $this->repository->createItem($request->all());
            return redirect(url('tipo_lideranca/itens', $objeto->tipo_lideranca_codigo_tipo_lideranca));
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update_item(Request $request) {
        try {
            $objeto = $this->repository->updateItem($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect(url('tipo_lideranca/itens', $objeto->tipo_lideranca_codigo_tipo_lideranca));
    }

    public function destroy_item(Request $request) {
        try {
            $this->repository->destroy_item($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
    }

}
