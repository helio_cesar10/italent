<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Database\Repositories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EspecializacaoController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    private $repository;

    public function __construct(Repositories\EspecializacaoRepository $repository) {
        $this->repository = $repository;
    }

    public function index() {
        $objetos = $this->repository->findAll();
        return view('especializacao.index', ['objetos' => $objetos]);
    }

    public function cadastro() {
        return view('especializacao.cadastro');
    }

    public function edita($id) {
        $objeto = $this->repository->findId($id);
        return view('especializacao.edita', ['objeto' => $objeto]);
    }

    public function create(Request $request) {
        $message = ['required' => ':attribute é obrigatório'];
        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $id = $this->repository->create($request->all());
            return redirect('especializacao');
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update(Request $request) {
        try {
            $this->repository->update($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect('especializacao');
    }

    public function destroy(Request $request) {
        try {
            $this->repository->destroy($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
//        return Response::json();
    }

    public function assuntos($id) {
        $objetos = $this->repository->findAllAssuntos($id);
        return view('especializacao.assuntos', ['id' => $id, 'objetos' => $objetos]);
    }

    public function cadastro_assunto($id) {
        return view('especializacao_assunto.cadastro_assunto', ['id' => $id]);
    }

    public function edita_assunto($id) {
        $objeto = $this->repository->findIdAssunto($id);
        return view('especializacao_assunto.edita_assunto', ['objeto' => $objeto]);
    }

    public function create_assunto(Request $request) {
        $message = ['required' => ':attribute é obrigatório'];
        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $objeto = $this->repository->createAssuntos($request->all());
            return redirect(url('especializacao/assunto', $objeto->especializacao_codigo_especializacao));
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update_assunto(Request $request) {
        try {
            $objeto = $this->repository->updateAssunto($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect(url('especializacao/assunto', $objeto->especializacao_codigo_especializacao));
    }

    public function destroy_assunto(Request $request) {
        try {
            $this->repository->destroy_assunto($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
    }

}
