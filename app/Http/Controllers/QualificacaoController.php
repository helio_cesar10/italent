<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Database\Repositories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class QualificacaoController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    private $repository;

    public function __construct(Repositories\QualificacaoRepository $repository) {
        $this->repository = $repository;
    }

    public function index() {
        $objetos = $this->repository->findAll();
        return view('qualificacao.index', ['objetos' => $objetos]);
    }

    public function cadastro() {
        return view('qualificacao.cadastro');
    }

    public function edita($id) {
        $objeto = $this->repository->findId($id);
        return view('qualificacao.edita', ['objeto' => $objeto]);
    }

    public function create(Request $request) {
        $message = ['required' => ':attribute é obrigatório'];
        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $id = $this->repository->create($request->all());
            return redirect('qualificacao');
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update(Request $request) {
        try {
            $this->repository->update($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect('qualificacao');
    }

    public function destroy(Request $request) {
        try {
            $this->repository->destroy($request->all());            
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
//        return Response::json();
    }

}
