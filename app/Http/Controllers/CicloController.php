<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Database\Repositories;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;

//use Illuminate\Http\Response;

class CicloController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    private $repository;

    public function __construct(
    Repositories\CicloRepository $repository, Repositories\CompetenciaRepository $repository_competencia, Repositories\EspecializacaoRepository $repository_especializacao, Repositories\QualificacaoRepository $repository_qualificacao, Repositories\TipoLiderancaRepository $repository_tipo_lideranca, Repositories\PessoaRepository $repository_pessoa) {
        $this->repository = $repository;
        $this->repository_competencia = $repository_competencia;
        $this->repository_especializacao = $repository_especializacao;
        $this->repository_qualificacao = $repository_qualificacao;
        $this->repository_tipo_lideranca = $repository_tipo_lideranca;
        $this->repository_pessoa = $repository_pessoa;
    }

    public function index() {
        $objetos = $this->repository->findAll();
        return view('ciclo.index', ['objetos' => $objetos]);
    }

    public function cadastro() {
        return view('ciclo.cadastro');
    }

    public function edita($id) {
        $objeto = $this->repository->findId($id);
        return view('ciclo.edita', ['objeto' => $objeto]);
    }

    public function create(Request $request) {
        $message = ['required' => ':attribute é obrigatório'];
        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $id = $this->repository->create($request->all());
            return redirect('ciclo');
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update(Request $request) {
        try {
            $this->repository->update($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect('ciclo');
    }

    public function destroy(Request $request) {
        try {
            $this->repository->destroy($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
    }

    public function itens($id) {

        $objeto = $this->repository->findId($id);
        $objetos = $this->repository->findAllItems($id);

        return view('ciclo.itens', ['id' => $id, 'objeto' => $objeto, 'objetos' => $objetos]);
    }

    public function cadastro_item($id) {
        $colaboradores = $this->repository->findAllColaboradores();
        $competencias = $this->repository_competencia->findAllCompetencia();
        $especializacao = $this->repository_especializacao->findAllEspecializacao();
        $qualificacao = $this->repository_qualificacao->findAllQualificacao();
        $tipo_lideranca = $this->repository_tipo_lideranca->findAllTipoLideranca();
        $objeto = $this->repository->findId($id);

        return view('ciclo_item.cadastro_item', [
            'id' => $id, 'objeto' => $objeto, 'colaboradores' => $colaboradores,
            'competencias' => $competencias, 'especializacao' => $especializacao,
            'tipo_lideranca' => $tipo_lideranca, 'qualificacao' => $qualificacao
        ]);
    }

    public function edita_item($id) {

        $objeto = $this->repository->findIdItem($id);

        $colaboradores = $this->repository->findAllColaboradores();
        $competencias = $this->repository_competencia->findAllCompetencia();
        $especializacao = $this->repository_especializacao->findAllEspecializacao();
        $qualificacao = $this->repository_qualificacao->findAllQualificacao();
        $tipo_lideranca = $this->repository_tipo_lideranca->findAllTipoLideranca();

        $competenciaSalvas = array();
        foreach ($objeto->cicloResultadoCompetencia as $cicloCompetencia) {
            $competenciaSalvas[] = $cicloCompetencia->competencia->codigo_competencia;
        }

        $especializacaoSalvas = array();
        foreach ($objeto->cicloResultadoEspecializacao as $cicloEspecializacao) {
            $especializacaoSalvas[] = $cicloEspecializacao->especializacao->codigo_especializacao;
        }
        $qualificacaoSalvas = array();
        foreach ($objeto->cicloResultadoQualificacao as $cicloQualificacao) {
            $qualificacaoSalvas[] = $cicloQualificacao->qualificacao->codigo_qualificacao;
        }
        $tipoLiderancaSalvas = array();
        foreach ($objeto->cicloResultadoTipoLideranca as $ciclotipoLideranca) {
            $tipoLiderancaSalvas[] = $ciclotipoLideranca->tipo_lideranca->codigo_tipo_lideranca;
        }

        return view('ciclo_item.edita_item', [
            'id' => $id, 'objeto' => $objeto, 'colaboradores' => $colaboradores,
            'competencias' => $competencias, 'especializacao' => $especializacao,
            'tipo_lideranca' => $tipo_lideranca, 'qualificacao' => $qualificacao,
            'competenciaSalvas' => $competenciaSalvas, 'especializacaoSalvas' => $especializacaoSalvas,
            'qualificacaoSalvas' => $qualificacaoSalvas, 'tipoLiderancaSalvas' => $tipoLiderancaSalvas
        ]);
    }

    public function create_item(Request $request) {
//        $message = ['required' => ':attribute é obrigatório'];
//        $this->validate($request, ['descricao' => 'required'], $message);
        try {
            $objeto = $this->repository->createItem($request->all());
            return redirect(url('ciclo/itens', $objeto->ciclo_codigo));
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
    }

    public function update_item(Request $request) {
        try {
            $objeto = $this->repository->updateItem($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return redirect(url('ciclo/itens', $objeto->ciclo_codigo));
    }

    public function destroy_item(Request $request) {
        try {
            $this->repository->destroy_item($request->all());
        } catch (Exception $ex) {
            \App::abort(500, $ex->getMessage());
        }
        return response()->json();
    }

    public function export($id) {
        $objetos = [];

        $data = $this->repository->findAllItemsExport($id)->toArray();
//        $data = $this->repository->findAllItemsExport($id);
        foreach ($data as $value) {
            if (empty($objetos)) {
                $objetos[] = $value;
            } else {
                $id = $this->searchForId($value['cpf'], $objetos);
                if ($id === null) {
                    $objetos[] = $value;
                } else {
                    if ($objetos[$id]['competencia'] != $value['competencia']) {
                        $string = $objetos[$id]['competencia'] . "," . $value['competencia'];
                        $objetos[$id]['competencia'] = $string;

                        $array1 = explode(',', $objetos[$id]['competencia']);
                        $array1 = array_unique($array1);
                        $array1 = implode(",", $array1);
                        $objetos[$id]['competencia'] = $array1;
                    }
                    if ($objetos[$id]['especializacao'] != $value['especializacao']) {
                        $string = $objetos[$id]['especializacao'] . "," . $value['especializacao'];
                        $objetos[$id]['especializacao'] = $string;

                        $array2 = explode(',', $objetos[$id]['especializacao']);
                        $array2 = array_unique($array2);
                        $array2 = implode(",", $array2);
                        $objetos[$id]['especializacao'] = $array2;
//                       
                    }
                    if ($objetos[$id]['qualificacao'] != $value['qualificacao']) {
                        $string = $objetos[$id]['qualificacao'] . "," . $value['qualificacao'];
                        $objetos[$id]['qualificacao'] = $string;

                        $array3 = explode(',', $objetos[$id]['qualificacao']);
                        $array3 = array_unique($array3);
                        $array3 = implode(",", $array3);
                        $objetos[$id]['qualificacao'] = $array3;
//                       11
                    }
                    if ($objetos[$id]['tipo_lideranca'] != $value['tipo_lideranca']) {
                        $string = $objetos[$id]['tipo_lideranca'] . "," . $value['tipo_lideranca'];
                        $objetos[$id]['tipo_lideranca'] = $string;

                        $array4 = explode(',', $objetos[$id]['tipo_lideranca']);
                        $array4 = array_unique($array4);
                        $array4 = implode(",", $array4);
                        $objetos[$id]['tipo_lideranca'] = $array4;
//                       
                    }
                }
            }
//            
//            dd($value['cpf']);
//            if (array_search($value['cpf'], array_column($objetos, "cpf"))) {
//                $objetos[] = $value;
//            }
        }

        return Excel::create('banco_talento', function($excel) use ($objetos) {
                    $excel->sheet('ITALENT', function($sheet) use ($objetos) {
                        $sheet->fromArray($objetos);
                    });
                })->download('xls');
    }

    function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['cpf'] === $id) {
                return $key;
            }
        }
        return null;
    }

    public function import($id) {
        return view('ciclo_item.import', ['codigo_ciclo' => $id]);
    }

    public function importRegistro() {
        $codigo_ciclo = Input::get('ciclo_codigo');

        Excel::load(Input::file('file'), function($reader) {
            $reader->each(function($sheet) {
                if (!empty($sheet->cpf)) {
                    $sheet->cpf = strval($sheet->cpf);
                    $pessoa = $this->repository_pessoa->findCpf($sheet->cpf);

//              se pessoa nao exite cadastra.
                    if (empty($pessoa)) {
                        $pessoa = $this->repository_pessoa->create($sheet);
                        $data['colaborador'] = $pessoa->codigo_pessoas;
                        $data['ciclo_codigo'] = Input::get('ciclo_codigo');

                        if (!empty($sheet->competencia)) {
                            $competencias = explode(',', $sheet->competencia);
                            $i = 0;
                            foreach ($competencias as $value) {
                                $competencia = $this->repository_competencia->findDescricao($value);
//                              competencia nao exite
                                if (empty($competencia)) {
                                    $data['descricao'] = $value;
                                    $competencia = $this->repository_competencia->create($data);

                                    $data["competencias"][$i] = $competencia->codigo_competencia;
                                } else {
                                    $data["competencias"][$i] = $competencia->codigo_competencia;
                                }
                                $i++;
                            }
                        }
                        if (!empty($sheet->especializacao)) {
                            $especializacoes = explode(',', $sheet->especializacao);
                            $i = 0;
                            foreach ($especializacoes as $value) {
                                $especializacao = $this->repository_especializacao->findDescricao($value);
//                          especializacao nao exite
                                if (empty($especializacao)) {
                                    $data['descricao'] = $value;
                                    $especializacao = $this->repository_especializacao->create($data);
                                    $data["especializacao"][$i] = $especializacao->codigo_especializacao;
                                } else {
                                    $data["especializacao"][$i] = $especializacao->codigo_especializacao;
                                }
                                $i++;
                            }
                        }

                        if (!empty($sheet->qualificacao)) {
                            $qualificacoes = explode(',', $sheet->qualificacao);
                            $i = 0;
                            foreach ($qualificacoes as $value) {
                                $qualificacao = $this->repository_qualificacao->findDescricao($value);
//                          especializacao nao exite
                                if (empty($qualificacao)) {
                                    $data['descricao'] = $value;
                                    $qualificacao = $this->repository_qualificacao->create($data);
                                    $data["qualificacao"][$i] = $qualificacao->codigo_qualificacao;
                                } else {
                                    $data["qualificacao"][$i] = $qualificacao->codigo_qualificacao;
                                }
                                $i++;
                            }
                        }

                        if (!empty($sheet->tipo_lideranca)) {
                            $tipo_liderancas = explode(',', $sheet->tipo_lideranca);
                            $i = 0;
                            foreach ($tipo_liderancas as $value) {
                                $tipo_lideranca = $this->repository_tipo_lideranca->findDescricao($value);
//                          especializacao nao exite
                                if (empty($tipo_lideranca)) {
                                    $data['descricao'] = $value;
                                    $tipo_lideranca = $this->repository_tipo_lideranca->create($data);
                                    $data["tipo_lideranca"][$i] = $tipo_lideranca->codigo_tipo_lideranca;
                                } else {
                                    $data["tipo_lideranca"][$i] = $tipo_lideranca->codigo_tipo_lideranca;
                                }
                                $i++;
                            }
                        }

                        $cicloResultado = $this->repository->createItem($data);
                    } else {

                        $data['colaborador'] = $pessoa->codigo_pessoas;
                        $data['ciclo_codigo'] = Input::get('ciclo_codigo');

                        if (!empty($sheet->competencia)) {
                            $competencias = explode(',', $sheet->competencia);
                            $i = 0;
                            foreach ($competencias as $value) {
                                $competencia = $this->repository_competencia->findDescricao($value);
//                              competencia nao exite
                                if (empty($competencia)) {
                                    $data['descricao'] = $value;
                                    $competencia = $this->repository_competencia->create($data);

                                    $data["competencias"][$i] = $competencia->codigo_competencia;
                                } else {
                                    $data["competencias"][$i] = $competencia->codigo_competencia;
                                }
                                $i++;
                            }
                        }
                        if (!empty($sheet->especializacao)) {
                            $especializacoes = explode(',', $sheet->especializacao);
                            $i = 0;
                            foreach ($especializacoes as $value) {
                                $especializacao = $this->repository_especializacao->findDescricao($value);
//                          especializacao nao exite
                                if (empty($especializacao)) {
                                    $data['descricao'] = $value;
                                    $especializacao = $this->repository_especializacao->create($data);
                                    $data["especializacao"][$i] = $especializacao->codigo_especializacao;
                                } else {
                                    $data["especializacao"][$i] = $especializacao->codigo_especializacao;
                                }
                                $i++;
                            }
                        }

                        if (!empty($sheet->qualificacao)) {
                            $qualificacoes = explode(',', $sheet->qualificacao);
                            $i = 0;
                            foreach ($qualificacoes as $value) {
                                $qualificacao = $this->repository_qualificacao->findDescricao($value);
//                          especializacao nao exite
                                if (empty($qualificacao)) {
                                    $data['descricao'] = $value;
                                    $qualificacao = $this->repository_qualificacao->create($data);
                                    $data["qualificacao"][$i] = $qualificacao->codigo_qualificacao;
                                } else {
                                    $data["qualificacao"][$i] = $qualificacao->codigo_qualificacao;
                                }
                                $i++;
                            }
                        }

                        if (!empty($sheet->tipo_lideranca)) {
                            $tipo_liderancas = explode(',', $sheet->tipo_lideranca);
                            $i = 0;
                            foreach ($tipo_liderancas as $value) {
                                $tipo_lideranca = $this->repository_tipo_lideranca->findDescricao($value);
//                          especializacao nao exite
                                if (empty($tipo_lideranca)) {
                                    $data['descricao'] = $value;
                                    $tipo_lideranca = $this->repository_tipo_lideranca->create($data);
                                    $data["tipo_lideranca"][$i] = $tipo_lideranca->codigo_tipo_lideranca;
                                } else {
                                    $data["tipo_lideranca"][$i] = $tipo_lideranca->codigo_tipo_lideranca;
                                }
                                $i++;
                            }
                        }

                        $cicloResultado = $this->repository->createItem($data);
                    }
                }
            });
        });
        return redirect(url('ciclo/itens', $codigo_ciclo));
    }

    public function talentos() {
        $objetos = new Collection();
        $qualificacao = $this->repository_qualificacao->findAllQualificacao();
        $competencias = $this->repository_competencia->findAllCompetencia();
        $especializacao = $this->repository_especializacao->findAllEspecializacao();
        $tipo_lideranca = $this->repository_tipo_lideranca->findAllTipoLideranca();
        $qualificacao['0'] = 'SELECIONE UM REGISTRO';
        $competencias['0'] = 'SELECIONE UM REGISTRO';
        $especializacao['0'] = 'SELECIONE UM REGISTRO';
        $tipo_lideranca['0'] = 'SELECIONE UM REGISTRO';
        $qualificacao->sort();
        $competencias->sortBy('');
        $especializacao->sortBy('');
        $tipo_lideranca->sortBy('');

        return view('ciclo.talentos', ['objetos' => $objetos, 'competencias' => $competencias,
            'especializacao' => $especializacao, 'tipo_lideranca' => $tipo_lideranca, 'qualificacao' => $qualificacao]);
    }

    public function postTalentos(Request $request) {
//        dd($request->all());
//        $objetos = [];
        $qualificacao = $this->repository_qualificacao->findAllQualificacao();
        $competencias = $this->repository_competencia->findAllCompetencia();
        $especializacao = $this->repository_especializacao->findAllEspecializacao();
        $tipo_lideranca = $this->repository_tipo_lideranca->findAllTipoLideranca();
        $qualificacao['0'] = 'SELECIONE UM REGISTRO';
        $competencias['0'] = 'SELECIONE UM REGISTRO';
        $especializacao['0'] = 'SELECIONE UM REGISTRO';
        $tipo_lideranca['0'] = 'SELECIONE UM REGISTRO';
        $qualificacao->sort();
        $competencias->sortBy('');
        $especializacao->sortBy('');
        $tipo_lideranca->sortBy('');

        $resutados = array();
        if (!empty($request->qualificacao) or $request->qualificacao != 0) {
            $res1 = $this->repository->cicloResultadoQualificacao($request->qualificacao);
            foreach ($res1 as $value) {
                $resutados[$value->ciclo_resultado_codigo] = $value->ciclo_resultado_codigo;
            }
        }

        if (!empty($request->perfil) or $request->perfil != 0) {
            $res2 = $this->repository->cicloResultadoEspecializacao($request->perfil);
            foreach ($res2 as $value) {
                $resutados[$value->ciclo_resultado_codigo] = $value->ciclo_resultado_codigo;
            }
        }

        if (!empty($request->competencias) or $request->competencias != 0) {
            $res3 = $this->repository->cicloResultadoCompetencia($request->competencias);
            foreach ($res3 as $value) {
                $resutados[$value->ciclo_resultado_codigo] = $value->ciclo_resultado_codigo;
            }
        }

        if (!empty($request->tipo_lideranca) or $request->tipo_lideranca != 0) {
            $res4 = $this->repository->cicloResultadoTipoLideranca($request->tipo_lideranca);
            foreach ($res4 as $value) {
                $resutados[$value->ciclo_resultado_codigo] = $value->ciclo_resultado_codigo;
            }
        }

        $objetos = $this->repository->findAllItemsId($request);

        return view('ciclo.talentos', ['objetos' => $objetos, 'competencias' => $competencias,
            'especializacao' => $especializacao, 'tipo_lideranca' => $tipo_lideranca, 'qualificacao' => $qualificacao]);
    }

    public function exportTalento($values) {
        $values = json_decode($values);
        $collectionion = new Collection();
        foreach ($values as $value) {
            $value = $this->repository->findAllItemsExportTalento($value->codigo_ciclo_resultado);
            $collectionion->push($value);
        }
        $objetoss = null;
        $objetos = [];
//        dd($collectionion);

        $i = 0;
        foreach ($collectionion as $value) {
            $objetos[$i] = $value;

            foreach ($value->cicloResultadoCompetencia as $cicloCompetencia) {
//                $objetos[$i]['competencia'] = $cicloCompetencia->competencia->descricao;
                $string = $objetos[$i]['competencia'] . "," . $cicloCompetencia->competencia->descricao;
                $objetos[$i]['competencia'] = $string;

                $array1 = explode(',', $objetos[$i]['competencia']);
                $array1 = array_unique($array1);
                $array1 = implode(",", $array1);
                $objetos[$i]['competencia'] = $array1;
            }

            foreach ($value->cicloResultadoQualificacao as $cicloQualificacao) {
//                $objetos[$i]['qualificacao'] = $cicloQualificacao->qualificacao->descricao;
                $string = $objetos[$i]['qualificacao'] . "," . $cicloQualificacao->qualificacao->descricao;
                $objetos[$i]['qualificacao'] = $string;

                $array3 = explode(',', $objetos[$i]['qualificacao']);
                $array3 = array_unique($array3);
                $array3 = implode(",", $array3);
                $objetos[$i]['qualificacao'] = $array3;
            }

            foreach ($value->cicloResultadoEspecializacao as $cicloEspecializacao) {
//                $objetos[$i]['especializacao'] = $cicloEspecializacao->especializacao->descricao;
                $string = $objetos[$i]['especializacao'] . "," . $cicloEspecializacao->especializacao->descricao;
                $objetos[$i]['especializacao'] = $string;

                $array2 = explode(',', $objetos[$i]['especializacao']);
                $array2 = array_unique($array2);
                $array2 = implode(",", $array2);
                $objetos[$i]['especializacao'] = $array2;
            }

            foreach ($value->cicloResultadoTipoLideranca as $cicloTipoLideranca) {
//                $objetos[$i]['tipo_lideranca'] = $cicloTipoLideranca->tipo_lideranca->descricao;
                $string = $objetos[$i]['tipo_lideranca'] . "," . $cicloTipoLideranca->tipo_lideranca->descricao;
                $objetos[$i]['tipo_lideranca'] = $string;

                $array4 = explode(',', $objetos[$i]['tipo_lideranca']);
                $array4 = array_unique($array4);
                $array4 = implode(",", $array4);
                $objetos[$i]['tipo_lideranca'] = $array4;
            }
            $objetos[$i]['nome'] = $value->colaborador->nome;
            $objetos[$i]['cpf'] = $value->colaborador->cpf;
            $objetos[$i]['matricula'] = $value->colaborador->matricula;
            $objetos[$i]['setor'] = $value->colaborador->setor;

            $objetoss[$i]['nome'] = $objetos[$i]['nome'];
            $objetoss[$i]['cpf'] = $objetos[$i]['cpf'];
            $objetoss[$i]['matricula'] = $objetos[$i]['matricula'];
            $objetoss[$i]['setor'] = $objetos[$i]['setor'];

            $objetoss[$i]['tipo_lideranca'] = $objetos[$i]['tipo_lideranca'];
            $objetoss[$i]['especializacao'] = $objetos[$i]['especializacao'];
            $objetoss[$i]['qualificacao'] = $objetos[$i]['qualificacao'];
            $objetoss[$i]['competencia'] = $objetos[$i]['competencia'];
            $i++;
        }
//        dd($objetoss);
        $array = json_decode(json_encode($objetoss), true);
        return Excel::create('banco_talento', function($excel) use ($array) {
                    $excel->sheet('ITALENT', function($sheet) use ($array) {
                        $sheet->fromArray($array);
                    });
                })->download('xls');
    }

    public function pdfTalento($values) {
//        $values = json_decode($values);
//        $collectionion = new Collection();
//        foreach ($values as $value) {
//            $value = $this->repository->findAllItemsExportTalento($value->codigo_ciclo_resultado);
//            $collectionion->push($value);
//        }
//        $objetoss = null;
//        $objetos = [];
////        dd($collectionion);
//
//        $i = 0;
//        foreach ($collectionion as $value) {
//            $objetos[$i] = $value;
//
//            foreach ($value->cicloResultadoCompetencia as $cicloCompetencia) {
////                $objetos[$i]['competencia'] = $cicloCompetencia->competencia->descricao;
//                $string = $objetos[$i]['competencia'] . "," . $cicloCompetencia->competencia->descricao;
//                $objetos[$i]['competencia'] = $string;
//
//                $array1 = explode(',', $objetos[$i]['competencia']);
//                $array1 = array_unique($array1);
//                $array1 = implode(",", $array1);
//                $objetos[$i]['competencia'] = $array1;
//            }
//
//            foreach ($value->cicloResultadoQualificacao as $cicloQualificacao) {
////                $objetos[$i]['qualificacao'] = $cicloQualificacao->qualificacao->descricao;
//                $string = $objetos[$i]['qualificacao'] . "," . $cicloQualificacao->qualificacao->descricao;
//                $objetos[$i]['qualificacao'] = $string;
//
//                $array3 = explode(',', $objetos[$i]['qualificacao']);
//                $array3 = array_unique($array3);
//                $array3 = implode(",", $array3);
//                $objetos[$i]['qualificacao'] = $array3;
//            }
//
//            foreach ($value->cicloResultadoEspecializacao as $cicloEspecializacao) {
////                $objetos[$i]['especializacao'] = $cicloEspecializacao->especializacao->descricao;
//                $string = $objetos[$i]['especializacao'] . "," . $cicloEspecializacao->especializacao->descricao;
//                $objetos[$i]['especializacao'] = $string;
//
//                $array2 = explode(',', $objetos[$i]['especializacao']);
//                $array2 = array_unique($array2);
//                $array2 = implode(",", $array2);
//                $objetos[$i]['especializacao'] = $array2;
//            }
//
//            foreach ($value->cicloResultadoTipoLideranca as $cicloTipoLideranca) {
////                $objetos[$i]['tipo_lideranca'] = $cicloTipoLideranca->tipo_lideranca->descricao;
//                $string = $objetos[$i]['tipo_lideranca'] . "," . $cicloTipoLideranca->tipo_lideranca->descricao;
//                $objetos[$i]['tipo_lideranca'] = $string;
//
//                $array4 = explode(',', $objetos[$i]['tipo_lideranca']);
//                $array4 = array_unique($array4);
//                $array4 = implode(",", $array4);
//                $objetos[$i]['tipo_lideranca'] = $array4;
//            }
//            $objetos[$i]['nome'] = $value->colaborador->nome;
//            $objetos[$i]['cpf'] = $value->colaborador->cpf;
//            $objetos[$i]['matricula'] = $value->colaborador->matricula;
//            $objetos[$i]['setor'] = $value->colaborador->setor;
//
//            $objetoss[$i]['nome'] = $objetos[$i]['nome'];
//            $objetoss[$i]['cpf'] = $objetos[$i]['cpf'];
//            $objetoss[$i]['matricula'] = $objetos[$i]['matricula'];
//            $objetoss[$i]['setor'] = $objetos[$i]['setor'];
//
//            $objetoss[$i]['tipo_lideranca'] = $objetos[$i]['tipo_lideranca'];
//            $objetoss[$i]['especializacao'] = $objetos[$i]['especializacao'];
//            $objetoss[$i]['qualificacao'] = $objetos[$i]['qualificacao'];
//            $objetoss[$i]['competencia'] = $objetos[$i]['competencia'];
//            $i++;
//        }
////        dd($objetoss);
//        $array = json_decode(json_encode($objetoss), true);
//        return Excel::create('banco_talento', function($excel) use ($array) {
//                    $excel->sheet('ITALENT', function($sheet) use ($array) {
//                        $sheet->setOrientation('landscape');
//                        $sheet->fromArray($array);
//                    });
//                })->download('pdf');;
        return \PDF::loadFile('http://www.github.com')->stream('github.pdf'); 
    }

}
